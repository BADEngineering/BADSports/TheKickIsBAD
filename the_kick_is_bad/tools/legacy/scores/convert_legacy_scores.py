# TheKickIsBAD: An open-source NCAA football statistics engine.
# Copyright (C) 2019  BAD Engineering LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import os
import json
import sys

def convert_legacy_scores(year, week, legacy_filename):

    year = int(year)
    week = int(week)

    scores = { 'games': [] }

    with open(legacy_filename) as file:
        line = file.readline().strip()
        while line:
            score_components = line.split(',')
            away_team = score_components[0]
            home_team = score_components[1]
            away_score = score_components[2]
            home_score = score_components[3]
            date = score_components[4]
            date_components = date.split('-')
            game_year = date_components[0]
            game_month = date_components[1]
            game_day = date_components[2]
            game_date = '{0:>02}-{1:>02}-{2}'.format(game_month, game_day, game_year)
            location = score_components[5]
            url = '/game/football' + score_components[14]
            game = {
                'game': {
                    'away': {
                        'names': {
                            'short': away_team,
                            'standard': away_team
                        },
                        'score': away_score
                    },
                    'gameState': 'final',
                    'home': {
                        'names': {
                            'short': home_team,
                            'standard': home_team
                        },
                        'score': home_score
                    },
                    'startDate': game_date,
                    'startTime': '12:00PM ET',
                    'url': url,
                    'location': location
                }
            }
            scores['games'].append(game)
            line = file.readline().strip()

    filename = '{0}\\{1}\\scores-{1}-{2:02}.json'.format(os.path.dirname(os.path.realpath(__file__)), year, week)
    with open(filename, 'w') as file:
        json.dump(scores, file, indent=2)

if __name__ == '__main__':
    convert_legacy_scores(sys.argv[1], sys.argv[2], sys.argv[3])
