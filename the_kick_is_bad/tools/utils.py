# TheKickIsBAD: An open-source NCAA football statistics engine.
# Copyright (C) 2021 BAD Engineering LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, realpath
import sys
root = dirname(dirname(realpath(__file__)))
sys.path.append(root)

# Standard imports
import json
import os


def get_abs_path(file):

    return os.path.dirname(os.path.realpath(file))

def read_json(filename):

    with open(filename, 'r') as file:
        dict = json.load(file)
        return dict

def write_json(dict, filename):

    os.makedirs(os.path.dirname(filename), exist_ok=True)
    with open(filename, 'w') as file:
        json.dump(dict, file, indent=2)

def write_bytes(data, filename):

    os.makedirs(os.path.dirname(filename), exist_ok=True)
    with open(filename, 'wb') as file:
        file.write(data)

def write_string(data, filename):

    os.makedirs(os.path.dirname(filename), exist_ok=True)
    with open(filename, 'w') as file:
        file.write(data)

def write_strings(data, filename):

    os.makedirs(os.path.dirname(filename), exist_ok=True)
    with open(filename, 'w') as file:
        for line in data:
            file.write(f"{line}\n")

def check_week(week, num_weeks):

    if type(week) is str:
        week = num_weeks + 1
        is_bowl = True
    elif week > num_weeks:
        raise Exception('Value of week should not exceed {0}. Did you mean "bowl"?'.format(num_weeks))
    else:
        is_bowl = False

    return week, is_bowl
