# TheKickIsBAD: An open-source NCAA football statistics engine.
# Copyright (C) 2019  BAD Engineering LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, realpath
import sys
root = dirname(dirname(realpath(__file__)))
sys.path.append(root)

# Standard imports
import time
from urllib.request import urlopen

# TheKickIsBAD imports
from teams.get_standard_team_name import get_standard_team_name


def download_team_rankings_stats(scores, num_retries=3):

    print('Downloading statistics from teamrankings.com...')

    team_rankings_stats = {}

    # Calculate the date of the last played game + 1 day
    # This is a safe date to download stats for the week
    download_date = calculate_download_date(scores)

    urls = []
    stats = []

    # Red Zone Attempts
    urls.append('https://www.teamrankings.com/college-football/stat/red-zone-scoring-attempts-per-game?date={0}'.format(download_date))
    stats.append('fbs red zone attempts')

    # Red Zone Scores
    urls.append('https://www.teamrankings.com/college-football/stat/red-zone-scores-per-game?date={0}'.format(download_date))
    stats.append('fbs red zone conversions')

    # Offsensive Touchdowns
    urls.append('https://www.teamrankings.com/college-football/stat/offensive-touchdowns-per-game?date={0}'.format(download_date))
    stats.append('fbs offensive touchdowns')
    
    # Offensive Points
    urls.append('https://www.teamrankings.com/college-football/stat/offensive-points-per-game?date={0}'.format(download_date))
    stats.append('fbs offensive points')

    # 1st Quarter Points
    urls.append('https://www.teamrankings.com/college-football/stat/1st-quarter-points-per-game?date={0}'.format(download_date))
    stats.append('fbs 1st quarter points')

    # 2nd Quarter Points
    urls.append('https://www.teamrankings.com/college-football/stat/2nd-quarter-points-per-game?date={0}'.format(download_date))
    stats.append('fbs 2nd quarter points')

    # 3rd Quarter Points
    urls.append('https://www.teamrankings.com/college-football/stat/3rd-quarter-points-per-game?date={0}'.format(download_date))
    stats.append('fbs 3rd quarter points')

    # 4th Quarter Points
    urls.append('https://www.teamrankings.com/college-football/stat/4th-quarter-points-per-game?date={0}'.format(download_date))
    stats.append('fbs 4th quarter points')

    # Overtime Points
    urls.append('https://www.teamrankings.com/college-football/stat/overtime-points-per-game?date={0}'.format(download_date))
    stats.append('fbs overtime points')

    # 1st Half Points
    urls.append('https://www.teamrankings.com/college-football/stat/1st-half-points-per-game?date={0}'.format(download_date))
    stats.append('fbs 1st half points')

    # 2nd Half Points
    urls.append('https://www.teamrankings.com/college-football/stat/2nd-half-points-per-game?date={0}'.format(download_date))
    stats.append('fbs 2nd half points')
    
    # Time of Possession Percentage
    urls.append('https://www.teamrankings.com/college-football/stat/time-of-possession-pct-net-of-ot?date={0}'.format(download_date))
    stats.append('fbs time of possession percentage')
    
    # First Quarter Time of Possession Percentage
    urls.append('https://www.teamrankings.com/college-football/stat/1st-quarter-time-of-possession-share-pct?date={0}'.format(download_date))
    stats.append('fbs 1st quarter time of possession percentage')
    
    # Second Quarter Time of Possession Percentage
    urls.append('https://www.teamrankings.com/college-football/stat/2nd-quarter-time-of-possession-share-pct?date={0}'.format(download_date))
    stats.append('fbs 2nd quarter time of possession percentage')
    
    # Third Quarter Time of Possession Percentage
    urls.append('https://www.teamrankings.com/college-football/stat/3rd-quarter-time-of-possession-share-pct?date={0}'.format(download_date))
    stats.append('fbs 3rd quarter time of possession percentage')
    
    # Fourth Quarter Time of Possession Percentage
    urls.append('https://www.teamrankings.com/college-football/stat/4th-quarter-time-of-possession-share-pct?date={0}'.format(download_date))
    stats.append('fbs 4th quarter time of possession percentage')
    
    # First Half Time of Possession Percentage
    urls.append('https://www.teamrankings.com/college-football/stat/1st-half-time-of-possession-share-pct?date={0}'.format(download_date))
    stats.append('fbs 1st half time of possession percentage')
    
    # Second Half Time of Possession Percentage
    urls.append('https://www.teamrankings.com/college-football/stat/2nd-half-time-of-possession-share-pct?date={0}'.format(download_date))
    stats.append('fbs 2nd half time of possession percentage')
    
    # Sacks
    urls.append('https://www.teamrankings.com/college-football/stat/sacks-per-game?date={0}'.format(download_date))
    stats.append('fbs sacks')
    
    # Team Passer Rating
    urls.append('https://www.teamrankings.com/college-football/stat/average-team-passer-rating?date={0}'.format(download_date))
    stats.append('fbs team passer rating')
    
    # Field Goal Attempts
    urls.append('https://www.teamrankings.com/college-football/stat/field-goal-attempts-per-game?date={0}'.format(download_date))
    stats.append('fbs field goal attempts')
    
    # Field Goals Made
    urls.append('https://www.teamrankings.com/college-football/stat/field-goals-made-per-game?date={0}'.format(download_date))
    stats.append('fbs field goals made')

    # Loop through configured stats and download
    stat_index = 1
    num_stats = len(urls)
    for i in range(0, num_stats):
        url = urls[i]
        stat = stats[i]

        # Display download progress
        stat_output = '...Stat {0}/{1}: {2}'.format(stat_index, num_stats, stat)
        stat_index += 1
        print(stat_output)

        # Download the raw HTML table of stats
        success = False
        retries = 0
        while retries < num_retries:
            try:
                web_response = urlopen(url)
                web_data = web_response.read()
                success = True
                break
            except Exception:
                print('......Error downloading team ranking stats, retrying...')
                retries += 1
                time.sleep(1)
        if not success:
            raise Exception('Failed to download team rankings stats')

        # Parse the HTML table for the last game stat
        team_rankings_stats = parse_stats(web_data.decode('utf-8'), stat, team_rankings_stats)

    return team_rankings_stats

def calculate_download_date(scores):
    
    # Track the latest date seen
    latest_year = 0
    latest_month = 0
    latest_day = 0

    # Loop through games to check date
    for game in scores['games']:

        # Split the date string into components
        date = game['game']['startDate'].split('-')
        month = int(date[0])
        day = int(date[1])
        year = int(date[2])

        # Update the latest date seen
        if year > latest_year:
            latest_year = year
            latest_month = month
            latest_day = day
        elif month > latest_month:
            latest_month = month
            latest_day = day
        elif day > latest_day:
            latest_day = day
    
    # Do some sanity checks on date values
    assert(latest_year > 0)
    assert(latest_month > 0)
    assert(latest_month <= 12)
    assert(latest_day > 0)
    assert(latest_day <= 31)

    # Get the last day of the month since we need to increment 1 day
    if latest_month == 1 or latest_month == 3 or latest_month == 5 or latest_month == 7 or latest_month == 8 or latest_month == 10 or latest_month == 13:
        last_day_of_month = 31
    elif latest_month == 4 or latest_month == 6 or latest_month == 9 or latest_month == 11:
        last_day_of_month = 30
    else:
        last_day_of_month = 28 # screw leap year anyway

    # Increment 1 day and check for overflow
    latest_day += 1
    if latest_day > last_day_of_month:
        latest_day = 1
        latest_month = month + 1
        if latest_month > 12:
            latest_month = 1
            latest_year += 1

    # Format the download date according to teamrankings.com URLs
    download_date = '{0}-{1:0>2d}-{2:0>2d}'.format(latest_year, latest_month, latest_day)

    return download_date

def parse_stats(web_data, stat, team_rankings_stats):

    more_stats = True
    while more_stats:

        # Do some really ugly HTML parsing to find the table entry
        start_marker = '<td'
        i_start = web_data.find(start_marker)
        if i_start == -1:
            more_stats = False
            continue
        web_data = web_data[i_start:]
        end_marker = '</tr'
        i_end = web_data.find(end_marker)
        if i_end == -1:
            i_end = len(web_data) - 1
        entry = web_data[:i_end]
        web_data = web_data[i_end:]
        if entry == '':
            more_stats = False
            continue
        start_marker = 'data-sort="'
        i_start = entry.find(start_marker)
        assert(i_start != -1)
        entry = entry[i_start + len(start_marker):]
        i_start = entry.find(start_marker)
        assert(i_start != -1)
        entry = entry[i_start + len(start_marker):]
        end_marker = '">'
        i_end = entry.find(end_marker)
        assert(i_end != -1)
        team_name = entry[:i_end]
        team_name = get_standard_team_name(team_name)
        i_start = entry.find(start_marker)
        assert(i_start != -1)
        entry = entry[i_start + len(start_marker):]
        i_start = entry.find(start_marker)
        assert(i_start != -1)
        entry = entry[i_start + len(start_marker):]
        i_start = entry.find(start_marker)
        assert(i_start != -1)
        entry = entry[i_start + len(start_marker):]
        i_end = entry.find(end_marker)
        assert(i_end != -1)
        stat_entry = entry[:i_end]

        # Convert the last game stat to a number
        if stat_entry == 'N/A' or stat_entry == '--':
            stat_entry = '0'
        stat_value = float(stat_entry)

        # Pack the stat into the output structure
        if team_name not in team_rankings_stats:
            team_rankings_stats[team_name] = {}
        team_rankings_stats[team_name][stat] = stat_value
    
    return team_rankings_stats
