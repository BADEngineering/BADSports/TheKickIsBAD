# TheKickIsBAD: An open-source NCAA football statistics engine.
# Copyright (C) 2019  BAD Engineering LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, realpath
import sys
root = dirname(dirname(realpath(__file__)))
sys.path.append(root)

# TheKickIsBAD imports
import tools.utils as utils
from scores.read_number_of_weeks import read_number_of_weeks


def read_ncaa_stats(year, week):
    
    # Check if the week is 'bowl' week
    num_weeks = read_number_of_weeks(year)
    week, _ = utils.check_week(week, num_weeks)

    # Open stats file with absolute path
    absolute_path = utils.get_abs_path(__file__)
    filename = '{0}/{1}/ncaa/ncaa_stats-{1}-{2:02}.json'.format(absolute_path, year, week)
    return utils.read_json(filename)
