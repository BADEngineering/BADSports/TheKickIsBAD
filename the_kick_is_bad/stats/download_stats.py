# TheKickIsBAD: An open-source NCAA football statistics engine.
# Copyright (C) 2019  BAD Engineering LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, realpath
import sys
root = dirname(dirname(realpath(__file__)))
sys.path.append(root)

# TheKickIsBAD imports
import tools.utils as utils
from stats.calculate_stats import calculate_stats
from stats.download_ncaa_stats import download_ncaa_stats
from stats.download_team_rankings_stats import download_team_rankings_stats
from scores.read_number_of_weeks import read_number_of_weeks
from scores.read_scores import read_scores


def download_stats(year, week):

    # Read the scores to know what teams to download stats for
    scores = read_scores(year, week)

    if type(week) is str:
        print('Downloading stats year {0}, bowl week...'.format(year))
    else:
        print('Downloading stats year {0}, week {1:02}...'.format(year, week))

    # Download game center stats from ncaa.com
    ncaa_stats = download_ncaa_stats(year, week, scores)

    # Download historical stats from teamrankings.com
    team_rankings_stats = download_team_rankings_stats(scores)

    # Calculate derived stats and splits and combine to one structure
    stats = calculate_stats(year, week, scores, ncaa_stats, team_rankings_stats)

    # Check if the week is 'bowl' week
    num_weeks = read_number_of_weeks(year)
    week, _ = utils.check_week(week, num_weeks)

    # Create the ncaa stats file with absolute path
    absolute_path = utils.get_abs_path(__file__)
    filename = '{0}/{1}/ncaa/ncaa_stats-{1}-{2:02}.json'.format(absolute_path, year, week)
    utils.write_json(ncaa_stats, filename)

    # Create the TeamRankings stats file with absolute path
    filename = '{0}/{1}/team_rankings/team_rankings_stats-{1}-{2:02}.json'.format(absolute_path, year, week)
    utils.write_json(team_rankings_stats, filename)
        
    # Create the stats file with absolute path
    filename = '{0}/{1}/stats-{1}-{2:02}.json'.format(absolute_path, year, week)
    utils.write_json(stats, filename)


if __name__ == '__main__':
    year = int(sys.argv[1])
    week = sys.argv[2]
    if week != 'bowl':
        week = int(week)
    download_stats(year, week)
