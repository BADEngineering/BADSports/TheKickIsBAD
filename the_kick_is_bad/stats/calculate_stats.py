# TheKickIsBAD: An open-source NCAA football statistics engine.
# Copyright (C) 2019  BAD Engineering LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, realpath
import sys
root = dirname(dirname(realpath(__file__)))
sys.path.append(root)

# TheKickIsBAD imports
import tools.utils as utils
from stats.read_stats import read_stats
from scores.read_number_of_weeks import read_number_of_weeks
from teams.read_teams import read_teams


def calculate_stats(year, week, scores, ncaa_stats, team_rankings_stats):

    print('Calculating combined statistics...')

    stats = {}

    # Check if the week is 'bowl' week
    num_weeks = read_number_of_weeks(year)
    week, _ = utils.check_week(week, num_weeks)
    
    teams, _ = read_teams(year)

    # home_stadiums = read_stadiums(year)
    
    # Read previous weeks stats and initialize for carry-over
    if week > 1:
        stats = read_stats(year, week - 1)
        for team in teams:
            reset_weekly_stats(stats[team])
    else:
        for team in teams:
            stats[team] = initialize_team_statistics()
        
    # Loop through games to calculate stats
    for game in scores['games']:

        # Only calculate stats for teams in finished games
        if game['game']['gameState'] != 'final':
            continue

        # Get away team information
        away_team = game['game']['away']['names']['standard']
        away_team_conference = teams[away_team]['conference']
        away_score = int(game['game']['away']['score'])
        away_team_has_winning_record = stats[away_team]['record']['wins']['season'] > stats[away_team]['record']['losses']['season']

        # Get home team information
        home_team = game['game']['home']['names']['standard']
        home_team_conference = teams[home_team]['conference']
        home_score = int(game['game']['home']['score'])
        home_team_has_winning_record = stats[home_team]['record']['wins']['season'] > stats[home_team]['record']['losses']['season']

        # Get the matchup string used to uniquely identify the game
        game_key = '{0} vs {1}'.format(away_team, home_team)

        # Calculate matchup information
        is_conference_game = away_team_conference == home_team_conference
        is_fbs_game = away_team != 'FCS' and home_team != 'FCS'
        away_team_won = away_score > home_score # ties are counted as losses in splits
        home_team_won = home_score > away_score # ties are counted as losses in splits
        if week <= num_weeks:
            is_home = True
        else:
            # print('...Detected neutral site for {0} @ {1}'.format(away_team, home_team))
            # print('......Home stadium: {0}'.format(home_stadiums[home_team]))
            # print('......Game location: {0}'.format(ncaa_stats[game_key]['stadium']))
            is_home = False

        # 'weekly'
        calculate_weekly_stats(stats[away_team]['weekly'], False, is_fbs_game, is_conference_game, home_team_has_winning_record, away_team_won)
        calculate_weekly_stats(stats[home_team]['weekly'], is_home, is_fbs_game, is_conference_game, away_team_has_winning_record, home_team_won)

        # 'schedule'
        calculate_schedule_stats(stats[away_team]['schedule'], home_team, week, False, is_conference_game, home_team_has_winning_record, away_team_won)
        calculate_schedule_stats(stats[home_team]['schedule'], away_team, week, is_home, is_conference_game, away_team_has_winning_record, home_team_won)
        
        # 'games played', 'fbs games played', 'record'
        calculate_record_stats(stats[away_team]['games played'], stats[away_team]['fbs games played'], stats[away_team]['record'], 
            False, is_conference_game, home_team_has_winning_record, is_fbs_game, away_team_won)
        
        calculate_record_stats(stats[home_team]['games played'], stats[home_team]['fbs games played'], stats[home_team]['record'], 
            is_home, is_conference_game, away_team_has_winning_record, is_fbs_game, home_team_won)
        
        # 'points'
        calculate_splits(stats[away_team]['points']['total'], away_score, home_score, False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[home_team]['points']['total'], home_score, away_score, is_home, is_conference_game, away_team_has_winning_record)
        if is_fbs_game:
            calculate_splits(stats[away_team]['points']['1st quarter'], team_rankings_stats[away_team]['fbs 1st quarter points'], team_rankings_stats[home_team]['fbs 1st quarter points'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[away_team]['points']['2nd quarter'], team_rankings_stats[away_team]['fbs 2nd quarter points'], team_rankings_stats[home_team]['fbs 2nd quarter points'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[away_team]['points']['3rd quarter'], team_rankings_stats[away_team]['fbs 3rd quarter points'], team_rankings_stats[home_team]['fbs 3rd quarter points'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[away_team]['points']['4th quarter'], team_rankings_stats[away_team]['fbs 4th quarter points'], team_rankings_stats[home_team]['fbs 4th quarter points'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[away_team]['points']['overtime'], team_rankings_stats[away_team]['fbs overtime points'], team_rankings_stats[home_team]['fbs overtime points'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[away_team]['points']['1st half'], team_rankings_stats[away_team]['fbs 1st half points'], team_rankings_stats[home_team]['fbs 1st half points'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[away_team]['points']['2nd half'], team_rankings_stats[away_team]['fbs 2nd half points'], team_rankings_stats[home_team]['fbs 2nd half points'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[home_team]['points']['1st quarter'], team_rankings_stats[home_team]['fbs 1st quarter points'], team_rankings_stats[away_team]['fbs 1st quarter points'], is_home, is_conference_game, away_team_has_winning_record)
            calculate_splits(stats[home_team]['points']['2nd quarter'], team_rankings_stats[home_team]['fbs 2nd quarter points'], team_rankings_stats[away_team]['fbs 2nd quarter points'], is_home, is_conference_game, away_team_has_winning_record)
            calculate_splits(stats[home_team]['points']['3rd quarter'], team_rankings_stats[home_team]['fbs 3rd quarter points'], team_rankings_stats[away_team]['fbs 3rd quarter points'], is_home, is_conference_game, away_team_has_winning_record)
            calculate_splits(stats[home_team]['points']['4th quarter'], team_rankings_stats[home_team]['fbs 4th quarter points'], team_rankings_stats[away_team]['fbs 4th quarter points'], is_home, is_conference_game, away_team_has_winning_record)
            calculate_splits(stats[home_team]['points']['overtime'], team_rankings_stats[home_team]['fbs overtime points'], team_rankings_stats[away_team]['fbs overtime points'], is_home, is_conference_game, away_team_has_winning_record)
            calculate_splits(stats[home_team]['points']['1st half'], team_rankings_stats[home_team]['fbs 1st half points'], team_rankings_stats[away_team]['fbs 1st half points'], is_home, is_conference_game, away_team_has_winning_record)
            calculate_splits(stats[home_team]['points']['2nd half'], team_rankings_stats[home_team]['fbs 2nd half points'], team_rankings_stats[away_team]['fbs 2nd half points'], is_home, is_conference_game, away_team_has_winning_record)

        # '1st downs'
        calculate_splits(stats[away_team]['1st downs']['total'], ncaa_stats[game_key][away_team]['1st downs'], ncaa_stats[game_key][home_team]['1st downs'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[away_team]['1st downs']['rushing'], ncaa_stats[game_key][away_team]['rushing 1st downs'], ncaa_stats[game_key][home_team]['rushing 1st downs'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[away_team]['1st downs']['passing'], ncaa_stats[game_key][away_team]['passing 1st downs'], ncaa_stats[game_key][home_team]['passing 1st downs'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[away_team]['1st downs']['penalty'], ncaa_stats[game_key][away_team]['penalty 1st downs'], ncaa_stats[game_key][home_team]['penalty 1st downs'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[home_team]['1st downs']['total'], ncaa_stats[game_key][home_team]['1st downs'], ncaa_stats[game_key][away_team]['1st downs'], is_home, is_conference_game, away_team_has_winning_record)
        calculate_splits(stats[home_team]['1st downs']['rushing'], ncaa_stats[game_key][home_team]['rushing 1st downs'], ncaa_stats[game_key][away_team]['rushing 1st downs'], is_home, is_conference_game, away_team_has_winning_record)
        calculate_splits(stats[home_team]['1st downs']['passing'], ncaa_stats[game_key][home_team]['passing 1st downs'], ncaa_stats[game_key][away_team]['passing 1st downs'], is_home, is_conference_game, away_team_has_winning_record)
        calculate_splits(stats[home_team]['1st downs']['penalty'], ncaa_stats[game_key][home_team]['penalty 1st downs'], ncaa_stats[game_key][away_team]['penalty 1st downs'], is_home, is_conference_game, away_team_has_winning_record)

        # 'rushing'
        calculate_splits(stats[away_team]['rushing']['yards'], ncaa_stats[game_key][away_team]['rushing yards'], ncaa_stats[game_key][home_team]['rushing yards'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[away_team]['rushing']['attempts'], ncaa_stats[game_key][away_team]['rushing attempts'], ncaa_stats[game_key][home_team]['rushing attempts'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[home_team]['rushing']['yards'], ncaa_stats[game_key][home_team]['rushing yards'], ncaa_stats[game_key][away_team]['rushing yards'], is_home, is_conference_game, away_team_has_winning_record)
        calculate_splits(stats[home_team]['rushing']['attempts'], ncaa_stats[game_key][home_team]['rushing attempts'], ncaa_stats[game_key][away_team]['rushing attempts'], is_home, is_conference_game, away_team_has_winning_record)

        # 'passing'
        calculate_splits(stats[away_team]['passing']['yards'], ncaa_stats[game_key][away_team]['passing yards'], ncaa_stats[game_key][home_team]['passing yards'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[away_team]['passing']['attempts'], ncaa_stats[game_key][away_team]['passing attempts'], ncaa_stats[game_key][home_team]['passing attempts'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[away_team]['passing']['completions'], ncaa_stats[game_key][away_team]['passing completions'], ncaa_stats[game_key][home_team]['passing completions'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[home_team]['passing']['yards'], ncaa_stats[game_key][home_team]['passing yards'], ncaa_stats[game_key][away_team]['passing yards'], is_home, is_conference_game, away_team_has_winning_record)
        calculate_splits(stats[home_team]['passing']['attempts'], ncaa_stats[game_key][home_team]['passing attempts'], ncaa_stats[game_key][away_team]['passing attempts'], is_home, is_conference_game, away_team_has_winning_record)
        calculate_splits(stats[home_team]['passing']['completions'], ncaa_stats[game_key][home_team]['passing completions'], ncaa_stats[game_key][away_team]['passing completions'], is_home, is_conference_game, away_team_has_winning_record)

        # 'total offense'
        calculate_splits(stats[away_team]['total offense']['yards'], ncaa_stats[game_key][away_team]['total yards'], ncaa_stats[game_key][home_team]['total yards'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[away_team]['total offense']['plays'], ncaa_stats[game_key][away_team]['total plays'], ncaa_stats[game_key][home_team]['total plays'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[home_team]['total offense']['yards'], ncaa_stats[game_key][home_team]['total yards'], ncaa_stats[game_key][away_team]['total yards'], is_home, is_conference_game, away_team_has_winning_record)
        calculate_splits(stats[home_team]['total offense']['plays'], ncaa_stats[game_key][home_team]['total plays'], ncaa_stats[game_key][away_team]['total plays'], is_home, is_conference_game, away_team_has_winning_record)

        # 'fumbles'
        calculate_splits(stats[away_team]['fumbles']['total'], ncaa_stats[game_key][away_team]['fumbles'], ncaa_stats[game_key][home_team]['fumbles'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[away_team]['fumbles']['lost'], ncaa_stats[game_key][away_team]['fumbles lost'], ncaa_stats[game_key][home_team]['fumbles lost'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[home_team]['fumbles']['total'], ncaa_stats[game_key][home_team]['fumbles'], ncaa_stats[game_key][away_team]['fumbles'], is_home, is_conference_game, away_team_has_winning_record)
        calculate_splits(stats[home_team]['fumbles']['lost'], ncaa_stats[game_key][home_team]['fumbles lost'], ncaa_stats[game_key][away_team]['fumbles lost'], is_home, is_conference_game, away_team_has_winning_record)

        # 'penalties'
        calculate_splits(stats[away_team]['penalties']['number'], ncaa_stats[game_key][away_team]['penalties'], ncaa_stats[game_key][home_team]['penalties'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[away_team]['penalties']['yards'], ncaa_stats[game_key][away_team]['penalty yards'], ncaa_stats[game_key][home_team]['penalty yards'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[home_team]['penalties']['number'], ncaa_stats[game_key][home_team]['penalties'], ncaa_stats[game_key][away_team]['penalties'], is_home, is_conference_game, away_team_has_winning_record)
        calculate_splits(stats[home_team]['penalties']['yards'], ncaa_stats[game_key][home_team]['penalty yards'], ncaa_stats[game_key][away_team]['penalty yards'], is_home, is_conference_game, away_team_has_winning_record)

        # 'punts'
        calculate_splits(stats[away_team]['punts']['number'], ncaa_stats[game_key][away_team]['punts'], ncaa_stats[game_key][home_team]['punts'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[away_team]['punts']['yards'], ncaa_stats[game_key][away_team]['punt yards'], ncaa_stats[game_key][home_team]['punt yards'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[home_team]['punts']['number'], ncaa_stats[game_key][home_team]['punts'], ncaa_stats[game_key][away_team]['punts'], is_home, is_conference_game, away_team_has_winning_record)
        calculate_splits(stats[home_team]['punts']['yards'], ncaa_stats[game_key][home_team]['punt yards'], ncaa_stats[game_key][away_team]['punt yards'], is_home, is_conference_game, away_team_has_winning_record)

        # 'punt returns'
        calculate_splits(stats[away_team]['punt returns']['number'], ncaa_stats[game_key][away_team]['punt returns'], ncaa_stats[game_key][home_team]['punt returns'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[away_team]['punt returns']['yards'], ncaa_stats[game_key][away_team]['punt return yards'], ncaa_stats[game_key][home_team]['punt return yards'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[home_team]['punt returns']['number'], ncaa_stats[game_key][home_team]['punt returns'], ncaa_stats[game_key][away_team]['punt returns'], is_home, is_conference_game, away_team_has_winning_record)
        calculate_splits(stats[home_team]['punt returns']['yards'], ncaa_stats[game_key][home_team]['punt return yards'], ncaa_stats[game_key][away_team]['punt return yards'], is_home, is_conference_game, away_team_has_winning_record)

        # 'kickoff returns'
        calculate_splits(stats[away_team]['kickoff returns']['number'], ncaa_stats[game_key][away_team]['kickoff returns'], ncaa_stats[game_key][home_team]['kickoff returns'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[away_team]['kickoff returns']['yards'], ncaa_stats[game_key][away_team]['kickoff return yards'], ncaa_stats[game_key][home_team]['kickoff return yards'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[home_team]['kickoff returns']['number'], ncaa_stats[game_key][home_team]['kickoff returns'], ncaa_stats[game_key][away_team]['kickoff returns'], is_home, is_conference_game, away_team_has_winning_record)
        calculate_splits(stats[home_team]['kickoff returns']['yards'], ncaa_stats[game_key][home_team]['kickoff return yards'], ncaa_stats[game_key][away_team]['kickoff return yards'], is_home, is_conference_game, away_team_has_winning_record)

        # 'interception returns'
        calculate_splits(stats[away_team]['interception returns']['number'], ncaa_stats[game_key][away_team]['interception returns'], ncaa_stats[game_key][home_team]['interception returns'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[away_team]['interception returns']['yards'], ncaa_stats[game_key][away_team]['interception return yards'], ncaa_stats[game_key][home_team]['interception return yards'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[home_team]['interception returns']['number'], ncaa_stats[game_key][home_team]['interception returns'], ncaa_stats[game_key][away_team]['interception returns'], is_home, is_conference_game, away_team_has_winning_record)
        calculate_splits(stats[home_team]['interception returns']['yards'], ncaa_stats[game_key][home_team]['interception return yards'], ncaa_stats[game_key][away_team]['interception return yards'], is_home, is_conference_game, away_team_has_winning_record)

        # '3rd downs'
        calculate_splits(stats[away_team]['3rd downs']['conversions'], ncaa_stats[game_key][away_team]['3rd down conversions'], ncaa_stats[game_key][home_team]['3rd down conversions'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[away_team]['3rd downs']['attempts'], ncaa_stats[game_key][away_team]['3rd down attempts'], ncaa_stats[game_key][home_team]['3rd down attempts'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[home_team]['3rd downs']['conversions'], ncaa_stats[game_key][home_team]['3rd down conversions'], ncaa_stats[game_key][away_team]['3rd down conversions'], is_home, is_conference_game, away_team_has_winning_record)
        calculate_splits(stats[home_team]['3rd downs']['attempts'], ncaa_stats[game_key][home_team]['3rd down attempts'], ncaa_stats[game_key][away_team]['3rd down attempts'], is_home, is_conference_game, away_team_has_winning_record)

        # '4th downs'
        calculate_splits(stats[away_team]['4th downs']['conversions'], ncaa_stats[game_key][away_team]['4th down conversions'], ncaa_stats[game_key][home_team]['4th down conversions'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[away_team]['4th downs']['attempts'], ncaa_stats[game_key][away_team]['4th down attempts'], ncaa_stats[game_key][home_team]['4th down attempts'], False, is_conference_game, home_team_has_winning_record)
        calculate_splits(stats[home_team]['4th downs']['conversions'], ncaa_stats[game_key][home_team]['4th down conversions'], ncaa_stats[game_key][away_team]['4th down conversions'], is_home, is_conference_game, away_team_has_winning_record)
        calculate_splits(stats[home_team]['4th downs']['attempts'], ncaa_stats[game_key][home_team]['4th down attempts'], ncaa_stats[game_key][away_team]['4th down attempts'], is_home, is_conference_game, away_team_has_winning_record)

        if is_fbs_game:

            # 'fbs red zone trips'
            calculate_splits(stats[away_team]['fbs red zone trips']['conversions'], team_rankings_stats[away_team]['fbs red zone conversions'], team_rankings_stats[home_team]['fbs red zone conversions'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[away_team]['fbs red zone trips']['attempts'], team_rankings_stats[away_team]['fbs red zone attempts'], team_rankings_stats[home_team]['fbs red zone attempts'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[home_team]['fbs red zone trips']['conversions'], team_rankings_stats[home_team]['fbs red zone conversions'], team_rankings_stats[away_team]['fbs red zone conversions'], is_home, is_conference_game, away_team_has_winning_record)
            calculate_splits(stats[home_team]['fbs red zone trips']['attempts'], team_rankings_stats[home_team]['fbs red zone attempts'], team_rankings_stats[away_team]['fbs red zone attempts'], is_home, is_conference_game, away_team_has_winning_record)

            # 'fbs offensive scoring'
            calculate_splits(stats[away_team]['fbs offensive scoring']['touchdowns'], team_rankings_stats[away_team]['fbs offensive touchdowns'], team_rankings_stats[home_team]['fbs offensive touchdowns'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[away_team]['fbs offensive scoring']['points'], team_rankings_stats[away_team]['fbs offensive points'], team_rankings_stats[home_team]['fbs offensive points'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[home_team]['fbs offensive scoring']['touchdowns'], team_rankings_stats[home_team]['fbs offensive touchdowns'], team_rankings_stats[away_team]['fbs offensive touchdowns'], is_home, is_conference_game, away_team_has_winning_record)
            calculate_splits(stats[home_team]['fbs offensive scoring']['points'], team_rankings_stats[home_team]['fbs offensive points'], team_rankings_stats[away_team]['fbs offensive points'], is_home, is_conference_game, away_team_has_winning_record)

            # 'fbs time of possession percentage'
            calculate_splits(stats[away_team]['fbs time of possession percentage']['total'], team_rankings_stats[away_team]['fbs time of possession percentage'], team_rankings_stats[home_team]['fbs time of possession percentage'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[away_team]['fbs time of possession percentage']['1st quarter'], team_rankings_stats[away_team]['fbs 1st quarter time of possession percentage'], team_rankings_stats[home_team]['fbs 1st quarter time of possession percentage'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[away_team]['fbs time of possession percentage']['2nd quarter'], team_rankings_stats[away_team]['fbs 2nd quarter time of possession percentage'], team_rankings_stats[home_team]['fbs 2nd quarter time of possession percentage'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[away_team]['fbs time of possession percentage']['3rd quarter'], team_rankings_stats[away_team]['fbs 3rd quarter time of possession percentage'], team_rankings_stats[home_team]['fbs 3rd quarter time of possession percentage'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[away_team]['fbs time of possession percentage']['4th quarter'], team_rankings_stats[away_team]['fbs 4th quarter time of possession percentage'], team_rankings_stats[home_team]['fbs 4th quarter time of possession percentage'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[away_team]['fbs time of possession percentage']['1st half'], team_rankings_stats[away_team]['fbs 1st half time of possession percentage'], team_rankings_stats[home_team]['fbs 1st half time of possession percentage'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[away_team]['fbs time of possession percentage']['2nd half'], team_rankings_stats[away_team]['fbs 2nd half time of possession percentage'], team_rankings_stats[home_team]['fbs 2nd half time of possession percentage'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[home_team]['fbs time of possession percentage']['total'], team_rankings_stats[home_team]['fbs time of possession percentage'], team_rankings_stats[away_team]['fbs time of possession percentage'], is_home, is_conference_game, away_team_has_winning_record)
            calculate_splits(stats[home_team]['fbs time of possession percentage']['1st quarter'], team_rankings_stats[home_team]['fbs 1st quarter time of possession percentage'], team_rankings_stats[away_team]['fbs 1st quarter time of possession percentage'], is_home, is_conference_game, away_team_has_winning_record)
            calculate_splits(stats[home_team]['fbs time of possession percentage']['2nd quarter'], team_rankings_stats[home_team]['fbs 2nd quarter time of possession percentage'], team_rankings_stats[away_team]['fbs 2nd quarter time of possession percentage'], is_home, is_conference_game, away_team_has_winning_record)
            calculate_splits(stats[home_team]['fbs time of possession percentage']['3rd quarter'], team_rankings_stats[home_team]['fbs 3rd quarter time of possession percentage'], team_rankings_stats[away_team]['fbs 3rd quarter time of possession percentage'], is_home, is_conference_game, away_team_has_winning_record)
            calculate_splits(stats[home_team]['fbs time of possession percentage']['4th quarter'], team_rankings_stats[home_team]['fbs 4th quarter time of possession percentage'], team_rankings_stats[away_team]['fbs 4th quarter time of possession percentage'], is_home, is_conference_game, away_team_has_winning_record)
            calculate_splits(stats[home_team]['fbs time of possession percentage']['1st half'], team_rankings_stats[home_team]['fbs 1st half time of possession percentage'], team_rankings_stats[away_team]['fbs 1st half time of possession percentage'], is_home, is_conference_game, away_team_has_winning_record)
            calculate_splits(stats[home_team]['fbs time of possession percentage']['2nd half'], team_rankings_stats[home_team]['fbs 2nd half time of possession percentage'], team_rankings_stats[away_team]['fbs 2nd half time of possession percentage'], is_home, is_conference_game, away_team_has_winning_record)

            # 'fbs sacks'
            calculate_splits(stats[away_team]['fbs sacks'], team_rankings_stats[away_team]['fbs sacks'], team_rankings_stats[home_team]['fbs sacks'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[home_team]['fbs sacks'], team_rankings_stats[home_team]['fbs sacks'], team_rankings_stats[away_team]['fbs sacks'], is_home, is_conference_game, away_team_has_winning_record)

            # 'fbs field goals'
            calculate_splits(stats[away_team]['fbs field goals']['attempts'], team_rankings_stats[away_team]['fbs field goal attempts'], team_rankings_stats[home_team]['fbs field goal attempts'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[away_team]['fbs field goals']['made'], team_rankings_stats[away_team]['fbs field goals made'], team_rankings_stats[home_team]['fbs field goals made'], False, is_conference_game, home_team_has_winning_record)
            calculate_splits(stats[home_team]['fbs field goals']['attempts'], team_rankings_stats[home_team]['fbs field goal attempts'], team_rankings_stats[away_team]['fbs field goal attempts'], is_home, is_conference_game, away_team_has_winning_record)
            calculate_splits(stats[home_team]['fbs field goals']['made'], team_rankings_stats[home_team]['fbs field goals made'], team_rankings_stats[away_team]['fbs field goals made'], is_home, is_conference_game, away_team_has_winning_record)
    
    # Loop through all teams and recalculate their opponents' records and opponents' opponents' records
    # Even if a team didn't play, their estimation can be updated with new info
    for team in teams:
        for opponent, is_home, is_away, is_vs_conference, is_vs_winning, is_vs_losing, game_won in zip(stats[team]['schedule']['opponents'], stats[team]['schedule']['home'], stats[team]['schedule']['away'], stats[team]['schedule']['vs conference'], stats[team]['schedule']['vs winning'], stats[team]['schedule']['vs losing'], stats[team]['schedule']['game won']):
            
            # Opponents' record
            if opponent == 'FCS':
                calculate_fcs_opponents_record(stats[team]['record']['opponent wins'], stats[team]['record']['opponent losses'], stats[opponent]['record'], stats[opponent]['games played'],
                    week, is_home, is_away, is_vs_conference, is_vs_winning, is_vs_losing, game_won)
            else:
                calculate_opponents_record(stats[team]['record']['opponent wins'], stats[team]['record']['opponent losses'], stats[opponent]['record'],
                    is_home, is_away, is_vs_conference, is_vs_winning, is_vs_losing, game_won)

            # Opponents' opponents' record
            for opponents_opponent in stats[opponent]['schedule']['opponents']:
                if opponents_opponent == 'FCS':
                    calculate_fcs_opponents_record(stats[team]['record']['opponents opponent wins'], stats[team]['record']['opponents opponent losses'], stats[opponents_opponent]['record'], stats[opponents_opponent]['games played'],
                        week, is_home, is_away, is_vs_conference, is_vs_winning, is_vs_losing, game_won)
                else:
                    calculate_opponents_record(stats[team]['record']['opponents opponent wins'], stats[team]['record']['opponents opponent losses'], stats[opponents_opponent]['record'],
                        is_home, is_away, is_vs_conference, is_vs_winning, is_vs_losing, game_won)
        
            # Last 3
            for opponents_opponent in stats[opponent]['schedule']['opponents'][max(0, len(stats[opponent]['schedule']['opponents']) - 3):]:
                if opponents_opponent == 'FCS':
                    calculate_last3_opponents_record_fcs(stats[team]['record']['opponents opponent wins'], stats[team]['record']['opponents opponent losses'], stats[opponents_opponent]['record'], stats[opponents_opponent]['games played'], week)
                else:
                    calculate_last3_opponents_record(stats[team]['record']['opponents opponent wins'], stats[team]['record']['opponents opponent losses'], stats[opponents_opponent]['record'])

        # Last 3
        for opponent in stats[team]['schedule']['opponents'][max(0, len(stats[team]['schedule']['opponents']) - 3):]:
            if opponent == 'FCS':
                calculate_last3_opponents_record_fcs(stats[team]['record']['opponent wins'], stats[team]['record']['opponent losses'], stats[opponent]['record'], stats[opponent]['games played'], week)
            else:
                calculate_last3_opponents_record(stats[team]['record']['opponent wins'], stats[team]['record']['opponent losses'], stats[opponent]['record'])
    
    return stats

def calculate_weekly_stats(weekly_stats, is_home, is_fbs_game, is_conference_game, opposing_team_has_winning_record, team_won):

    weekly_stats['games played'] += 1

    if is_fbs_game:
        weekly_stats['fbs games played'] += 1

    if is_home:
        weekly_stats['home games played'] += 1
        if is_fbs_game:
            weekly_stats['fbs home games played'] += 1
    else:
        weekly_stats['away games played'] += 1
        if is_fbs_game:
            weekly_stats['fbs away games played'] += 1

    if is_conference_game:
        weekly_stats['games played vs conference'] += 1

    if opposing_team_has_winning_record:
        weekly_stats['games played vs winning'] += 1
        if is_fbs_game:
            weekly_stats['fbs games played vs winning'] += 1
    else:
        weekly_stats['games played vs losing'] += 1
        if is_fbs_game:
            weekly_stats['fbs games played vs losing'] += 1

    if team_won:
        weekly_stats['games won'] += 1
    else:
        weekly_stats['games lost'] += 1

def calculate_schedule_stats(schedule_stats, opposing_team, week, is_home, is_conference_game, opposing_team_has_winning_record, team_won):

    schedule_stats['opponents'].append(opposing_team)
    schedule_stats['week'].append(week)

    if is_home:
        schedule_stats['away'].append(0)
        schedule_stats['home'].append(1)
    else:
        schedule_stats['away'].append(1)
        schedule_stats['home'].append(0)

    if is_conference_game:
        schedule_stats['vs conference'].append(1)
    else:
        schedule_stats['vs conference'].append(0)

    if opposing_team_has_winning_record:
        schedule_stats['vs winning'].append(1)
        schedule_stats['vs losing'].append(0)
    else:
        schedule_stats['vs winning'].append(0)
        schedule_stats['vs losing'].append(1)

    if team_won:
        schedule_stats['game won'].append(1)
        schedule_stats['game lost'].append(0)
    else:
        schedule_stats['game won'].append(0)
        schedule_stats['game lost'].append(1)

def calculate_record_stats(games_played_stats, fbs_games_played_stats, record_stats, 
            is_home, is_conference_game, opposing_team_has_winning_record, is_fbs_game, team_won):

    games_played_stats['season'] += 1
    if is_fbs_game:
        fbs_games_played_stats['season'] += 1

    if games_played_stats['last 3'] < 3:
        games_played_stats['last 3'] += 1
    if is_fbs_game:
        if fbs_games_played_stats['last 3'] < 3:
            fbs_games_played_stats['last 3'] += 1

    if is_home:
        games_played_stats['home'] += 1
        if is_fbs_game:
            fbs_games_played_stats['home'] += 1
    else:
        games_played_stats['away'] += 1
        if is_fbs_game:
            fbs_games_played_stats['away'] += 1

    if is_conference_game:
        games_played_stats['vs conference'] += 1
        if is_fbs_game:
            fbs_games_played_stats['vs conference'] += 1

    if opposing_team_has_winning_record:
        games_played_stats['vs winning'] += 1
        if is_fbs_game:
            fbs_games_played_stats['vs winning'] += 1
    else:
        games_played_stats['vs losing'] += 1
        if is_fbs_game:
            fbs_games_played_stats['vs losing'] += 1

    if team_won:
        record_stats['wins']['season'] += 1
        record_stats['wins']['last 3'][2] = record_stats['wins']['last 3'][1]
        record_stats['wins']['last 3'][1] = record_stats['wins']['last 3'][0]
        record_stats['wins']['last 3'][0] = 1
        if is_home:
            record_stats['wins']['home'] += 1
        else:
            record_stats['wins']['away'] += 1
        if is_conference_game:
            record_stats['wins']['vs conference'] += 1
        if opposing_team_has_winning_record:
            record_stats['wins']['vs winning'] += 1
        else:
            record_stats['wins']['vs losing'] += 1
    else:
        record_stats['losses']['season'] += 1
        record_stats['losses']['last 3'][2] = record_stats['losses']['last 3'][1]
        record_stats['losses']['last 3'][1] = record_stats['losses']['last 3'][0]
        record_stats['losses']['last 3'][0] = 1
        if is_home:
            record_stats['losses']['home'] += 1
        else:
            record_stats['losses']['away'] += 1
        if is_conference_game:
            record_stats['losses']['vs conference'] += 1
        if opposing_team_has_winning_record:
            record_stats['losses']['vs winning'] += 1
        else:
            record_stats['losses']['vs losing'] += 1

def calculate_splits(current_stats, new_stat_gained, new_stat_allowed, is_home, is_vs_conference, is_vs_winning):

    # Calculate stat gained
    current_stats['gained'].append(new_stat_gained)

    # Calculate stat allowed
    current_stats['allowed'].append(new_stat_allowed)

def calculate_fcs_opponents_record(opponents_wins_stats, opponents_losses_stats, opponent_records_stats, opponent_games_played_stats,
                    week, is_home, is_away, is_vs_conference, is_vs_winning, is_vs_losing, game_won):

    # season
    normalized_fcs_wins = opponent_records_stats['wins']['season'] / opponent_games_played_stats['season'] * week
    normalized_fcs_losses = opponent_records_stats['losses']['season'] / opponent_games_played_stats['season'] * week
    opponents_wins_stats['season'] += round(normalized_fcs_wins)
    opponents_losses_stats['season'] += round(normalized_fcs_losses)

    # home
    if is_home:
        opponents_wins_stats['home'] += round(normalized_fcs_wins)
        opponents_losses_stats['home'] += round(normalized_fcs_losses)

    # away
    if is_away:
        opponents_wins_stats['away'] += round(normalized_fcs_wins)
        opponents_losses_stats['away'] += round(normalized_fcs_losses)

    # vs conference
    if is_vs_conference:
        opponents_wins_stats['vs conference'] += round(normalized_fcs_wins)
        opponents_losses_stats['vs conference'] += round(normalized_fcs_losses)

    # vs winning
    if is_vs_winning:
        opponents_wins_stats['vs winning'] += round(normalized_fcs_wins)
        opponents_losses_stats['vs winning'] += round(normalized_fcs_losses)

    # vs losing
    if is_vs_losing:
        opponents_wins_stats['vs losing'] += round(normalized_fcs_wins)
        opponents_losses_stats['vs losing'] += round(normalized_fcs_losses)

    # in wins/losses
    if game_won:
        opponents_wins_stats['in wins'] += round(normalized_fcs_wins)
        opponents_losses_stats['in wins'] += round(normalized_fcs_losses)
    else:
        opponents_wins_stats['in losses'] += round(normalized_fcs_wins)
        opponents_losses_stats['in losses'] += round(normalized_fcs_losses)

def calculate_opponents_record(opponents_wins_stats, opponents_losses_stats, opponent_records_stats,
                    is_home, is_away, is_vs_conference, is_vs_winning, is_vs_losing, game_won):

    # season
    opponents_wins_stats['season'] += opponent_records_stats['wins']['season']
    opponents_losses_stats['season'] += opponent_records_stats['losses']['season']

    # home
    if is_home:
        opponents_wins_stats['home'] += opponent_records_stats['wins']['season']
        opponents_losses_stats['home'] += opponent_records_stats['losses']['season']

    # away
    if is_away:
        opponents_wins_stats['away'] += opponent_records_stats['wins']['season']
        opponents_losses_stats['away'] += opponent_records_stats['losses']['season']

    # vs conference
    if is_vs_conference:
        opponents_wins_stats['vs conference'] += opponent_records_stats['wins']['season']
        opponents_losses_stats['vs conference'] += opponent_records_stats['losses']['season']

    # vs winning
    if is_vs_winning:
        opponents_wins_stats['vs winning'] += opponent_records_stats['wins']['season']
        opponents_losses_stats['vs winning'] += opponent_records_stats['losses']['season']

    # vs losing
    if is_vs_losing:
        opponents_wins_stats['vs losing'] += opponent_records_stats['wins']['season']
        opponents_losses_stats['vs losing'] += opponent_records_stats['losses']['season']

    # in wins/losses
    if game_won:
        opponents_wins_stats['in wins'] += opponent_records_stats['wins']['season']
        opponents_losses_stats['in wins'] += opponent_records_stats['losses']['season']
    else:
        opponents_wins_stats['in losses'] += opponent_records_stats['wins']['season']
        opponents_losses_stats['in losses'] += opponent_records_stats['losses']['season']

def calculate_last3_opponents_record_fcs(opponents_wins_stats, opponents_losses_stats, opponent_records_stats, opponent_games_played_stats, week):
    normalized_fcs_wins = opponent_records_stats['wins']['season'] / opponent_games_played_stats['season'] * week
    normalized_fcs_losses = opponent_records_stats['losses']['season'] / opponent_games_played_stats['season'] * week
    opponents_wins_stats['last 3'][2] = opponents_wins_stats['last 3'][1]
    opponents_wins_stats['last 3'][1] = opponents_wins_stats['last 3'][0]
    opponents_wins_stats['last 3'][0] = round(normalized_fcs_wins)
    opponents_losses_stats['last 3'][2] = opponents_losses_stats['last 3'][1]
    opponents_losses_stats['last 3'][1] = opponents_losses_stats['last 3'][0]
    opponents_losses_stats['last 3'][0] = round(normalized_fcs_losses)

def calculate_last3_opponents_record(opponents_wins_stats, opponents_losses_stats, opponent_records_stats):
    opponents_wins_stats['last 3'][2] = opponents_wins_stats['last 3'][1]
    opponents_wins_stats['last 3'][1] = opponents_wins_stats['last 3'][0]
    opponents_wins_stats['last 3'][0] = opponent_records_stats['wins']['season']
    opponents_losses_stats['last 3'][2] = opponents_losses_stats['last 3'][1]
    opponents_losses_stats['last 3'][1] = opponents_losses_stats['last 3'][0]
    opponents_losses_stats['last 3'][0] = opponent_records_stats['losses']['season']

def initialize_team_statistics():

    team_stats = {
        'weekly': {
            'games played': 0,
            'home games played': 0,
            'away games played': 0,
            'games played vs conference': 0,
            'games played vs winning': 0,
            'games played vs losing': 0,
            'fbs games played': 0,
            'fbs home games played': 0,
            'fbs away games played': 0,
            'fbs games played vs winning': 0,
            'fbs games played vs losing': 0,
            'games won': 0,
            'games lost': 0,
        },
        'schedule': {
            'opponents': [],
            'week': [],
            'home': [],
            'away': [],
            'vs conference': [],
            'vs winning': [],
            'vs losing': [],
            'game won': [],
            'game lost': []
        },
        'games played': {
            'season': 0,
            'last 3': 0,
            'home': 0,
            'away': 0,
            'vs conference': 0,
            'vs winning': 0,
            'vs losing': 0
        },
        'fbs games played': {
            'season': 0,
            'last 3': 0,
            'home': 0,
            'away': 0,
            'vs conference': 0,
            'vs winning': 0,
            'vs losing': 0,
        },
        'record': {
            'wins': {
                'season': 0,
                'last 3': [0, 0, 0],
                'home': 0,
                'away': 0,
                'vs conference': 0,
                'vs winning': 0,
                'vs losing': 0
            },
            'losses': {
                'season': 0,
                'last 3': [0, 0, 0],
                'home': 0,
                'away': 0,
                'vs conference': 0,
                'vs winning': 0,
                'vs losing': 0
            },
            'opponent wins': {
                'season': 0,
                'last 3': [0, 0, 0],
                'home': 0,
                'away': 0,
                'vs conference': 0,
                'vs winning': 0,
                'vs losing': 0,
                'in wins': 0,
                'in losses': 0
            },
            'opponent losses': {
                'season': 0,
                'last 3': [0, 0, 0],
                'home': 0,
                'away': 0,
                'vs conference': 0,
                'vs winning': 0,
                'vs losing': 0,
                'in wins': 0,
                'in losses': 0
            },
            'opponents opponent wins': {
                'season': 0,
                'last 3': [0, 0, 0],
                'home': 0,
                'away': 0,
                'vs conference': 0,
                'vs winning': 0,
                'vs losing': 0,
                'in wins': 0,
                'in losses': 0
            },
            'opponents opponent losses': {
                'season': 0,
                'last 3': [0, 0, 0],
                'home': 0,
                'away': 0,
                'vs conference': 0,
                'vs winning': 0,
                'vs losing': 0,
                'in wins': 0,
                'in losses': 0
            }
        },
        'points': {
            'total': {
                'gained': [],
                'allowed': [],
            },
            '1st quarter': {
                'gained': [],
                'allowed': [],
            },
            '2nd quarter': {
                'gained': [],
                'allowed': [],
            },
            '3rd quarter': {
                'gained': [],
                'allowed': [],
            },
            '4th quarter': {
                'gained': [],
                'allowed': [],
            },
            'overtime': {
                'gained': [],
                'allowed': [],
            },
            '1st half': {
                'gained': [],
                'allowed': [],
            },
            '2nd half': {
                'gained': [],
                'allowed': [],
            }
        },
        '1st downs': {
            'total': {
                'gained': [],
                'allowed': [],
            },
            'rushing': {
                'gained': [],
                'allowed': [],
            },
            'passing': {
                'gained': [],
                'allowed': [],
            },
            'penalty': {
                'gained': [],
                'allowed': [],
            }
        },
        'rushing': {
            'yards': {
                'gained': [],
                'allowed': [],
            },
            'attempts': {
                'gained': [],
                'allowed': [],
            }
        },
        'passing': {
            'yards': {
                'gained': [],
                'allowed': [],
            },
            'attempts': {
                'gained': [],
                'allowed': [],
            },
            'completions': {
                'gained': [],
                'allowed': [],
            }
        },
        'total offense': {
            'yards': {
                'gained': [],
                'allowed': [],
            },
            'plays': {
                'gained': [],
                'allowed': [],
            }
        },
        'fumbles': {
            'total': {
                'gained': [],
                'allowed': [],
            },
            'lost': {
                'gained': [],
                'allowed': [],
            }
        },
        'penalties': {
            'number': {
                'gained': [],
                'allowed': [],
            },
            'yards': {
                'gained': [],
                'allowed': [],
            }
        },
        'punts': {
            'number': {
                'gained': [],
                'allowed': [],
            },
            'yards': {
                'gained': [],
                'allowed': [],
            }
        },
        'punt returns': {
            'number': {
                'gained': [],
                'allowed': [],
            },
            'yards': {
                'gained': [],
                'allowed': [],
            }
        },
        'kickoff returns': {
            'number': {
                'gained': [],
                'allowed': [],
            },
            'yards': {
                'gained': [],
                'allowed': [],
            }
        },
        'interception returns': {
            'number': {
                'gained': [],
                'allowed': [],
            },
            'yards': {
                'gained': [],
                'allowed': [],
            }
        },
        '3rd downs': {
            'conversions': {
                'gained': [],
                'allowed': [],
            },
            'attempts': {
                'gained': [],
                'allowed': [],
            }
        },
        '4th downs': {
            'conversions': {
                'gained': [],
                'allowed': [],
            },
            'attempts': {
                'gained': [],
                'allowed': [],
            }
        },
        'fbs red zone trips': {
            'conversions': {
                'gained': [],
                'allowed': [],
            },
            'attempts': {
                'gained': [],
                'allowed': [],
            }
        },
        'fbs offensive scoring': {
            'touchdowns': {
                'gained': [],
                'allowed': [],
            },
            'points': {
                'gained': [],
                'allowed': [],
            }
        },
        'fbs time of possession percentage': {
            'total': {
                'gained': [],
                'allowed': [],
            },
            '1st quarter': {
                'gained': [],
                'allowed': [],
            },
            '2nd quarter': {
                'gained': [],
                'allowed': [],
            },
            '3rd quarter': {
                'gained': [],
                'allowed': [],
            },
            '4th quarter': {
                'gained': [],
                'allowed': [],
            },
            '1st half': {
                'gained': [],
                'allowed': [],
            },
            '2nd half': {
                'gained': [],
                'allowed': [],
            }
        },
        'fbs sacks': {
            'gained': [],
            'allowed': [],
        },
        'fbs field goals': {
            'attempts': {
                'gained': [],
                'allowed': [],
            },
            'made': {
                'gained': [],
                'allowed': [],
            }
        },
    }

    return team_stats

def reset_weekly_stats(stats):

    stats['weekly']['games played'] = 0
    stats['weekly']['home games played'] = 0
    stats['weekly']['away games played'] = 0
    stats['weekly']['games played vs conference'] = 0
    stats['weekly']['games played vs winning'] = 0
    stats['weekly']['games played vs losing'] = 0
    stats['weekly']['fbs games played'] = 0
    stats['weekly']['fbs home games played'] = 0
    stats['weekly']['fbs away games played'] = 0
    stats['weekly']['fbs games played vs winning'] = 0
    stats['weekly']['fbs games played vs losing'] = 0
    stats['weekly']['games won'] = 0
    stats['weekly']['games lost'] = 0
    stats['record']['opponent wins']['season'] = 0
    stats['record']['opponent wins']['last 3'] = [0, 0, 0]
    stats['record']['opponent wins']['home'] = 0
    stats['record']['opponent wins']['away'] = 0
    stats['record']['opponent wins']['vs conference'] = 0
    stats['record']['opponent wins']['vs winning'] = 0
    stats['record']['opponent wins']['vs losing'] = 0
    stats['record']['opponent wins']['in wins'] = 0
    stats['record']['opponent wins']['in losses'] = 0
    stats['record']['opponent losses']['season'] = 0
    stats['record']['opponent losses']['last 3'] = [0, 0, 0]
    stats['record']['opponent losses']['home'] = 0
    stats['record']['opponent losses']['away'] = 0
    stats['record']['opponent losses']['vs conference'] = 0
    stats['record']['opponent losses']['vs winning'] = 0
    stats['record']['opponent losses']['vs losing'] = 0
    stats['record']['opponent losses']['in wins'] = 0
    stats['record']['opponent losses']['in losses'] = 0
    stats['record']['opponents opponent wins']['season'] = 0
    stats['record']['opponents opponent wins']['last 3'] = [0, 0, 0]
    stats['record']['opponents opponent wins']['home'] = 0
    stats['record']['opponents opponent wins']['away'] = 0
    stats['record']['opponents opponent wins']['vs conference'] = 0
    stats['record']['opponents opponent wins']['vs winning'] = 0
    stats['record']['opponents opponent wins']['vs losing'] = 0
    stats['record']['opponents opponent wins']['in wins'] = 0
    stats['record']['opponents opponent wins']['in losses'] = 0
    stats['record']['opponents opponent losses']['season'] = 0
    stats['record']['opponents opponent losses']['last 3'] = [0, 0, 0]
    stats['record']['opponents opponent losses']['home'] = 0
    stats['record']['opponents opponent losses']['away'] = 0
    stats['record']['opponents opponent losses']['vs conference'] = 0
    stats['record']['opponents opponent losses']['vs winning'] = 0
    stats['record']['opponents opponent losses']['vs losing'] = 0
    stats['record']['opponents opponent losses']['in wins'] = 0
    stats['record']['opponents opponent losses']['in losses'] = 0
