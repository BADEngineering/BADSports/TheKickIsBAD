# TheKickIsBAD: An open-source NCAA football statistics engine.
# Copyright (C) 2019  BAD Engineering LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, realpath
import sys
root = dirname(dirname(realpath(__file__)))
sys.path.append(root)

# Standard imports
import json
import re
import time
from urllib.request import urlopen

# TheKickIsBAD imports
import tools.utils as utils
from scores.read_number_of_weeks import read_number_of_weeks
from teams.read_teams import read_teams


def download_ncaa_stats(year, week, scores, num_retries=3):

    print('Downloading statistics from ncaa.com...')

    ncaa_stats = {}

    # Check if the week is 'bowl' week
    num_weeks = read_number_of_weeks(year)
    week, _ = utils.check_week(week, num_weeks)

    # Read the teams list to get conference info
    teams, _ = read_teams(year)

    # Loop through all games to game center download stats
    game_index = 1
    num_games = len(scores['games'])
    for game in scores['games']:

        # Get away team information
        away_team = game['game']['away']['names']['standard']
        away_team_nickname = game['game']['away']['names']['short'].strip()
        away_team_conference = teams[away_team]['conference']

        # Get home team information
        home_team = game['game']['home']['names']['standard']
        home_team_nickname = game['game']['home']['names']['short'].strip()
        home_team_conference = teams[home_team]['conference']

        # Display download progress
        game_output = '...Game {0}/{1}: {2} ({3}) @ {4} ({5})'.format(
            game_index, num_games, away_team_nickname, away_team_conference, home_team_nickname, home_team_conference)
        game_index += 1
        print(game_output)

        # Throw exception on old games that are FCS vs FCS due to ncaa.com quirks
        # They should be removed from the scores file so they aren't predicted
        if away_team == 'FCS' and home_team == 'FCS':
            raise Exception('Found an FCS @ FCS matchup, please remove this game from the scores file')

        # Get the matchup string used to uniquely identify the game
        game_key = '{0} vs {1}'.format(away_team, home_team)

        # Only download stats for finished games
        if game['game']['gameState'] == 'final':

            # if away_team == 'Michigan' and home_team == 'Connecticut':
            #     from read_ncaa_stats import read_ncaa_stats
            #     existing_ncaa_stats = read_ncaa_stats(year, week)
            #     ncaa_stats[game_key] = existing_ncaa_stats[game_key]
            #     continue
            # if away_team == 'Syracuse' and home_team == 'Connecticut':
            #     from read_ncaa_stats import read_ncaa_stats
            #     existing_ncaa_stats = read_ncaa_stats(year, week)
            #     ncaa_stats[game_key] = existing_ncaa_stats[game_key]
            #     continue

            # Get the game center URLs based on year
            # ncaa.com switched to "casablanca" in 2019
            game_center_url = game['game']['url']
            game_info_url = 'https://data.ncaa.com/casablanca{0}/gameInfo.json'.format(game_center_url)
            if year < 2019:
                team_stats_url = 'https://data.ncaa.com{0}/teamStats.json'.format(game_center_url)
            else:
                team_stats_url = 'https://data.ncaa.com/casablanca{0}/teamStats.json'.format(game_center_url)
            
            # Download the stats from the gameInfo page (mostly points)
            score_key = 'score'
            success = False
            retries = 0
            while retries < num_retries:
                try:

                    # Try downloading with the expected URL
                    game_info = download_game_info(game_info_url)
                    success = True
                    break
                except Exception:
                    
                    # If that fails, try downloading with the old style URL
                    alternate_game_info_url = 'https://data.ncaa.com{0}/gameinfo.json'.format(game_center_url)
                    try:
                        game_info = download_game_info(alternate_game_info_url)
                        score_key = 'currentScore'
                        success = True
                        break
                    except Exception:
                        print('......Error downloading game info, retrying...')
                        retries += 1
                        time.sleep(1)
            if not success:
                raise Exception('Failed to download game info')

            # Download the stats from the teamStats page (other categories)
            success = False
            retries = 0
            while retries < num_retries:
                try:

                    # Try downloading with the expected URL
                    team_stats = download_team_stats(team_stats_url)
                    success = True
                    break
                except Exception:

                    # If that fails, try downloading with the new style URL if it's 2018
                    if year == 2018:
                        alternate_team_stats_url = 'https://data.ncaa.com/casablanca{0}/teamStats.json'.format(game_center_url)
                        try:
                            team_stats = download_team_stats(alternate_team_stats_url)
                            success = True
                            break
                        except Exception:
                            print('......Error downloading team stats, retrying...')
                            retries += 1
                            time.sleep(1)
                    else:
                        print('......Error downloading team stats, retrying...')
                        retries += 1
                        time.sleep(1)
            if not success:
                raise Exception('Failed to download team stats')

            # Initialize empty stats if the game key doesn't already exist
            if game_key not in ncaa_stats:
                ncaa_stats[game_key] = initialize_ncaa_stats(away_team, home_team)

            # Pack game location
            if score_key == 'score':
                translation_table = str.maketrans('', '', '/-&() .,@;\'\n\t\r')
                try:
                    stadium = game_info['venue']['name'].translate(translation_table)
                    city = game_info['venue']['city'].translate(translation_table)
                    state = game_info['venue']['state'].translate(translation_table)
                    stadium_full_name = '{0}-{1}-{2}'.format(stadium, city, state)
                except Exception:
                    stadium_full_name = 'FCS-FCS-FCS'
                    print(f"Using FCS stadium for {home_team}")
                ncaa_stats[game_key]['stadium'] = stadium_full_name
            else:
                translation_table = str.maketrans('', '', '/-&() .@;\'\n\t\r')
                stadium = game_info['location'].translate(translation_table)
                translation_table = str.maketrans(',', '-')
                ncaa_stats[game_key]['stadium'] = stadium.translate(translation_table)

            # Pack away team gameInfo and teamStats data into the output structure
            ncaa_stats[game_key][away_team]['points'] += int(game_info['away'][score_key])
            first_down_stats = team_stats['teams'][0]['stats'][0]['data']
            if re.match(r'[0-9]+', first_down_stats):
                ncaa_stats[game_key][away_team]['1st downs'] += int(first_down_stats)
            else:
                print('......{0} is missing basic stats'.format(away_team))
            rushing_first_down_stats = team_stats['teams'][0]['stats'][0]['breakdown'][0]['data']
            if re.match(r'[0-9]+', rushing_first_down_stats):
                ncaa_stats[game_key][away_team]['rushing 1st downs'] += int(rushing_first_down_stats)
            passing_first_down_stats = team_stats['teams'][0]['stats'][0]['breakdown'][1]['data']
            if re.match(r'[0-9]+', passing_first_down_stats):
                ncaa_stats[game_key][away_team]['passing 1st downs'] += int(passing_first_down_stats)
            penalty_first_down_stats = team_stats['teams'][0]['stats'][0]['breakdown'][2]['data']
            if re.match(r'[0-9]+', penalty_first_down_stats):
                ncaa_stats[game_key][away_team]['penalty 1st downs'] += int(penalty_first_down_stats)
            rushing_yards_stats = team_stats['teams'][0]['stats'][1]['data']
            if re.match(r'[0-9]+', rushing_yards_stats):
                ncaa_stats[game_key][away_team]['rushing yards'] += int(rushing_yards_stats)
            rushing_attempts_stats = team_stats['teams'][0]['stats'][1]['breakdown'][0]['data']
            if re.match(r'[0-9]+', rushing_attempts_stats):
                ncaa_stats[game_key][away_team]['rushing attempts'] += int(rushing_attempts_stats)
            passing_yards_stats = team_stats['teams'][0]['stats'][2]['data']
            if re.match(r'[0-9]+', passing_yards_stats):
                ncaa_stats[game_key][away_team]['passing yards'] += int(passing_yards_stats)
            passing_attempts_stats = team_stats['teams'][0]['stats'][2]['breakdown'][0]['data']
            if re.match(r'[0-9]+', passing_attempts_stats):
                ncaa_stats[game_key][away_team]['passing attempts'] += int(passing_attempts_stats)
            passing_completions_stats = team_stats['teams'][0]['stats'][2]['breakdown'][1]['data']
            if re.match(r'[0-9]+', passing_completions_stats):
                ncaa_stats[game_key][away_team]['passing completions'] += int(passing_completions_stats)
            passing_interceptions_stats = team_stats['teams'][0]['stats'][2]['breakdown'][2]['data']
            if re.match(r'[0-9]+', passing_interceptions_stats):
                ncaa_stats[game_key][away_team]['passing interceptions'] += int(passing_interceptions_stats)
            total_yards_stats = team_stats['teams'][0]['stats'][3]['data']
            if re.match(r'[0-9]+', total_yards_stats):
                ncaa_stats[game_key][away_team]['total yards'] += int(total_yards_stats)
            total_plays_stats = team_stats['teams'][0]['stats'][3]['breakdown'][0]['data']
            if re.match(r'[0-9]+', total_plays_stats):
                ncaa_stats[game_key][away_team]['total plays'] += int(total_plays_stats)
            fumble_stats = team_stats['teams'][0]['stats'][4]['data']
            if re.match(r'[0-9]+\-[0-9]+', fumble_stats):
                fumble_stats = fumble_stats.split('-')
                ncaa_stats[game_key][away_team]['fumbles'] += int(fumble_stats[0])
                ncaa_stats[game_key][away_team]['fumbles lost'] += int(fumble_stats[1])
            else:
                print('......{0} is missing advanced stats'.format(away_team))
            penalty_stats = team_stats['teams'][0]['stats'][5]['data']
            if re.match(r'[0-9]+\--?[0-9]+', penalty_stats):
                penalty_stats = penalty_stats.split('-', 1)
                ncaa_stats[game_key][away_team]['penalties'] += int(penalty_stats[0])
                ncaa_stats[game_key][away_team]['penalty yards'] += int(penalty_stats[1])
            punt_stats = team_stats['teams'][0]['stats'][6]['data']
            if re.match(r'[0-9]+\--?[0-9]+', punt_stats):
                punt_stats = punt_stats.split('-', 1)
                ncaa_stats[game_key][away_team]['punts'] += int(punt_stats[0])
                ncaa_stats[game_key][away_team]['punt yards'] += int(punt_stats[1])
            punt_return_stats = team_stats['teams'][0]['stats'][7]['data']
            if re.match(r'[0-9]+\--?[0-9]+', punt_return_stats):
                punt_return_stats = punt_return_stats.split('-', 1)
                ncaa_stats[game_key][away_team]['punt returns'] += int(punt_return_stats[0])
                ncaa_stats[game_key][away_team]['punt return yards'] += int(punt_return_stats[1])
            kickoff_return_stats = team_stats['teams'][0]['stats'][8]['data']
            if re.match(r'[0-9]+\--?[0-9]+', kickoff_return_stats):
                kickoff_return_stats = kickoff_return_stats.split('-', 1)
                ncaa_stats[game_key][away_team]['kickoff returns'] += int(kickoff_return_stats[0])
                ncaa_stats[game_key][away_team]['kickoff return yards'] += int(kickoff_return_stats[1])
            interception_return_stats = team_stats['teams'][0]['stats'][9]['data']
            if re.match(r'[0-9]+\--?[0-9]+', interception_return_stats):
                interception_return_stats = interception_return_stats.split('-', 1)
                ncaa_stats[game_key][away_team]['interception returns'] += int(interception_return_stats[0])
                ncaa_stats[game_key][away_team]['interception return yards'] += int(interception_return_stats[1])
            third_down_stats = team_stats['teams'][0]['stats'][10]['data']
            if re.match(r'[0-9]+\-[0-9]+', third_down_stats):
                third_down_stats = third_down_stats.split('-')
                ncaa_stats[game_key][away_team]['3rd down conversions'] += int(third_down_stats[0])
                ncaa_stats[game_key][away_team]['3rd down attempts'] += int(third_down_stats[1])
            fourth_down_stats = team_stats['teams'][0]['stats'][11]['data']
            if re.match(r'[0-9]+\-[0-9]+', fourth_down_stats):
                fourth_down_stats = fourth_down_stats.split('-')
                ncaa_stats[game_key][away_team]['4th down conversions'] += int(fourth_down_stats[0])
                ncaa_stats[game_key][away_team]['4th down attempts'] += int(fourth_down_stats[1])

            # Pack home team gameInfo and teamStats data into the output structure
            ncaa_stats[game_key][home_team]['points'] += int(game_info['home'][score_key])
            first_down_stats = team_stats['teams'][1]['stats'][0]['data']
            if re.match(r'[0-9]+', first_down_stats):
                ncaa_stats[game_key][home_team]['1st downs'] += int(first_down_stats)
            else:
                print('......{0} is missing basic stats'.format(home_team))
            rushing_first_down_stats = team_stats['teams'][1]['stats'][0]['breakdown'][0]['data']
            if re.match(r'[0-9]+', rushing_first_down_stats):
                ncaa_stats[game_key][home_team]['rushing 1st downs'] += int(rushing_first_down_stats)
            passing_first_down_stats = team_stats['teams'][1]['stats'][0]['breakdown'][1]['data']
            if re.match(r'[0-9]+', passing_first_down_stats):
                ncaa_stats[game_key][home_team]['passing 1st downs'] += int(passing_first_down_stats)
            penalty_first_down_stats = team_stats['teams'][1]['stats'][0]['breakdown'][2]['data']
            if re.match(r'[0-9]+', penalty_first_down_stats):
                ncaa_stats[game_key][home_team]['penalty 1st downs'] += int(penalty_first_down_stats)
            rushing_yards_stats = team_stats['teams'][1]['stats'][1]['data']
            if re.match(r'[0-9]+', rushing_yards_stats):
                ncaa_stats[game_key][home_team]['rushing yards'] += int(rushing_yards_stats)
            rushing_attempts_stats = team_stats['teams'][1]['stats'][1]['breakdown'][0]['data']
            if re.match(r'[0-9]+', rushing_attempts_stats):
                ncaa_stats[game_key][home_team]['rushing attempts'] += int(rushing_attempts_stats)
            passing_yards_stats = team_stats['teams'][1]['stats'][2]['data']
            if re.match(r'[0-9]+', passing_yards_stats):
                ncaa_stats[game_key][home_team]['passing yards'] += int(passing_yards_stats)
            passing_attempts_stats = team_stats['teams'][1]['stats'][2]['breakdown'][0]['data']
            if re.match(r'[0-9]+', passing_attempts_stats):
                ncaa_stats[game_key][home_team]['passing attempts'] += int(passing_attempts_stats)
            passing_completions_stats = team_stats['teams'][1]['stats'][2]['breakdown'][1]['data']
            if re.match(r'[0-9]+', passing_completions_stats):
                ncaa_stats[game_key][home_team]['passing completions'] += int(passing_completions_stats)
            passing_interceptions_stats = team_stats['teams'][1]['stats'][2]['breakdown'][2]['data']
            if re.match(r'[0-9]+', passing_interceptions_stats):
                ncaa_stats[game_key][home_team]['passing interceptions'] += int(passing_interceptions_stats)
            total_yards_stats = team_stats['teams'][1]['stats'][3]['data']
            if re.match(r'[0-9]+', total_yards_stats):
                ncaa_stats[game_key][home_team]['total yards'] += int(total_yards_stats)
            total_plays_stats = team_stats['teams'][1]['stats'][3]['breakdown'][0]['data']
            if re.match(r'[0-9]+', total_plays_stats):
                ncaa_stats[game_key][home_team]['total plays'] += int(total_plays_stats)
            fumble_stats = team_stats['teams'][1]['stats'][4]['data']
            if re.match(r'[0-9]+\-[0-9]+', fumble_stats):
                fumble_stats = fumble_stats.split('-')
                ncaa_stats[game_key][home_team]['fumbles'] += int(fumble_stats[0])
                ncaa_stats[game_key][home_team]['fumbles lost'] += int(fumble_stats[1])
            else:
                print('......{0} is missing advanced stats'.format(home_team))
            penalty_stats = team_stats['teams'][1]['stats'][5]['data']
            if re.match(r'[0-9]+\--?[0-9]+', penalty_stats):
                penalty_stats = penalty_stats.split('-', 1)
                ncaa_stats[game_key][home_team]['penalties'] += int(penalty_stats[0])
                ncaa_stats[game_key][home_team]['penalty yards'] += int(penalty_stats[1])
            punt_stats = team_stats['teams'][1]['stats'][6]['data']
            if re.match(r'[0-9]+\--?[0-9]+', punt_stats):
                punt_stats = punt_stats.split('-', 1)
                ncaa_stats[game_key][home_team]['punts'] += int(punt_stats[0])
                ncaa_stats[game_key][home_team]['punt yards'] += int(punt_stats[1])
            punt_return_stats = team_stats['teams'][1]['stats'][7]['data']
            if re.match(r'[0-9]+\--?[0-9]+', punt_return_stats):
                punt_return_stats = punt_return_stats.split('-', 1)
                ncaa_stats[game_key][home_team]['punt returns'] += int(punt_return_stats[0])
                ncaa_stats[game_key][home_team]['punt return yards'] += int(punt_return_stats[1])
            kickoff_return_stats = team_stats['teams'][1]['stats'][8]['data']
            if re.match(r'[0-9]+\--?[0-9]+', kickoff_return_stats):
                kickoff_return_stats = kickoff_return_stats.split('-', 1)
                ncaa_stats[game_key][home_team]['kickoff returns'] += int(kickoff_return_stats[0])
                ncaa_stats[game_key][home_team]['kickoff return yards'] += int(kickoff_return_stats[1])
            interception_return_stats = team_stats['teams'][1]['stats'][9]['data']
            if re.match(r'[0-9]+\--?[0-9]+', interception_return_stats):
                interception_return_stats = interception_return_stats.split('-', 1)
                ncaa_stats[game_key][home_team]['interception returns'] += int(interception_return_stats[0])
                ncaa_stats[game_key][home_team]['interception return yards'] += int(interception_return_stats[1])
            third_down_stats = team_stats['teams'][1]['stats'][10]['data']
            if re.match(r'[0-9]+\-[0-9]+', third_down_stats):
                third_down_stats = third_down_stats.split('-')
                ncaa_stats[game_key][home_team]['3rd down conversions'] += int(third_down_stats[0])
                ncaa_stats[game_key][home_team]['3rd down attempts'] += int(third_down_stats[1])
            fourth_down_stats = team_stats['teams'][1]['stats'][11]['data']
            if re.match(r'[0-9]+\-[0-9]+', fourth_down_stats):
                fourth_down_stats = fourth_down_stats.split('-')
                ncaa_stats[game_key][home_team]['4th down conversions'] += int(fourth_down_stats[0])
                ncaa_stats[game_key][home_team]['4th down attempts'] += int(fourth_down_stats[1])
        
    return ncaa_stats

def download_game_info(game_info_url):
    web_response = urlopen(game_info_url)
    web_data = web_response.read()
    game_info = json.loads(web_data)
    return game_info

def download_team_stats(team_stats_url):
    web_response = urlopen(team_stats_url)
    web_data = web_response.read()
    team_stats = json.loads(web_data)
    return team_stats

def initialize_ncaa_stats(away_team, home_team):
    ncaa_stats = {
        'stadium': '',
        away_team: {
            'points': 0,
            '1st downs': 0,
            'rushing 1st downs': 0,
            'passing 1st downs': 0,
            'penalty 1st downs': 0,
            'rushing yards': 0,
            'rushing attempts': 0,
            'passing yards': 0,
            'passing attempts': 0,
            'passing completions': 0,
            'passing interceptions': 0,
            'total yards': 0,
            'total plays': 0,
            'fumbles': 0,
            'fumbles lost': 0,
            'penalties': 0,
            'penalty yards': 0,
            'punts': 0,
            'punt yards': 0,
            'punt returns': 0,
            'punt return yards': 0,
            'kickoff returns': 0,
            'kickoff return yards': 0,
            'interception returns': 0,
            'interception return yards': 0,
            '3rd down conversions': 0,
            '3rd down attempts': 0,
            '4th down conversions': 0,
            '4th down attempts': 0
        },
        home_team: {
            'points': 0,
            '1st downs': 0,
            'rushing 1st downs': 0,
            'passing 1st downs': 0,
            'penalty 1st downs': 0,
            'rushing yards': 0,
            'rushing attempts': 0,
            'passing yards': 0,
            'passing attempts': 0,
            'passing completions': 0,
            'passing interceptions': 0,
            'total yards': 0,
            'total plays': 0,
            'fumbles': 0,
            'fumbles lost': 0,
            'penalties': 0,
            'penalty yards': 0,
            'punts': 0,
            'punt yards': 0,
            'punt returns': 0,
            'punt return yards': 0,
            'kickoff returns': 0,
            'kickoff return yards': 0,
            'interception returns': 0,
            'interception return yards': 0,
            '3rd down conversions': 0,
            '3rd down attempts': 0,
            '4th down conversions': 0,
            '4th down attempts': 0
        }
    }

    return ncaa_stats
