# TheKickIsBAD: An open-source NCAA football statistics engine.
# Copyright (C) 2019  BAD Engineering LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, realpath
import sys
root = dirname(dirname(realpath(__file__)))
sys.path.append(root)

# TheKickIsBAD imports
import tools.utils as utils
from stats.read_stats import read_stats
from stats.read_ncaa_stats import read_ncaa_stats
from scores.read_number_of_weeks import read_number_of_weeks


def patch_stats(year, week, away_team, home_team):

    stats = read_stats(year, week)
    ncaa_stats = read_ncaa_stats(year, week)

    # Open patch file with absolute path
    absolute_path = utils.get_abs_path(__file__)
    filename = '{0}/{1}/patches/{1}-{2:02}-{3}-{4}.csv'.format(absolute_path, year, week, away_team, home_team)
    with open(filename) as file:

        is_conference = int(file.readline().strip())
        is_neutral_site = int(file.readline().strip())

        teams = file.readline().strip().split(',')
        away_team = teams[0]
        home_team = teams[1]
        game_key = '{0} vs {1}'.format(away_team, home_team)

        is_winning = file.readline().strip().split(',')
        away_is_winning = int(is_winning[0])
        home_is_winning = int(is_winning[1])

        # Total first downs
        total_first_downs = file.readline().strip().split(',')
        away_total_first_downs = int(total_first_downs[0])
        home_total_first_downs = int(total_first_downs[1])
        pack_stats(stats[away_team]['1st downs']['total'], away_total_first_downs, home_total_first_downs)
        pack_stats(stats[home_team]['1st downs']['total'], home_total_first_downs, away_total_first_downs)
        ncaa_stats[game_key][away_team]['1st downs'] = away_total_first_downs
        ncaa_stats[game_key][home_team]['1st downs'] = home_total_first_downs

        # Rushing first downs
        rushing_first_downs = file.readline().strip().split(',')
        away_rushing_first_downs = int(rushing_first_downs[0])
        home_rushing_first_downs = int(rushing_first_downs[1])
        pack_stats(stats[away_team]['1st downs']['rushing'], away_rushing_first_downs, home_rushing_first_downs)
        pack_stats(stats[home_team]['1st downs']['rushing'], home_rushing_first_downs, away_rushing_first_downs)
        ncaa_stats[game_key][away_team]['rushing 1st downs'] = away_rushing_first_downs
        ncaa_stats[game_key][home_team]['rushing 1st downs'] = home_rushing_first_downs

        # Passing first downs
        passing_first_downs = file.readline().strip().split(',')
        away_passing_first_downs = int(passing_first_downs[0])
        home_passing_first_downs = int(passing_first_downs[1])
        pack_stats(stats[away_team]['1st downs']['passing'], away_passing_first_downs, home_passing_first_downs)
        pack_stats(stats[home_team]['1st downs']['passing'], home_passing_first_downs, away_passing_first_downs)
        ncaa_stats[game_key][away_team]['passing 1st downs'] = away_passing_first_downs
        ncaa_stats[game_key][home_team]['passing 1st downs'] = home_passing_first_downs

        # Penalty first downs
        penalty_first_downs = file.readline().strip().split(',')
        away_penalty_first_downs = int(penalty_first_downs[0])
        home_penalty_first_downs = int(penalty_first_downs[1])
        pack_stats(stats[away_team]['1st downs']['penalty'], away_penalty_first_downs, home_penalty_first_downs)
        pack_stats(stats[home_team]['1st downs']['penalty'], home_penalty_first_downs, away_penalty_first_downs)
        ncaa_stats[game_key][away_team]['penalty 1st downs'] = away_penalty_first_downs
        ncaa_stats[game_key][home_team]['penalty 1st downs'] = home_penalty_first_downs

        # Rushing yards
        rushing_yards = file.readline().strip().split(',')
        away_rushing_yards = int(rushing_yards[0])
        home_rushing_yards = int(rushing_yards[1])
        pack_stats(stats[away_team]['rushing']['yards'], away_rushing_yards, home_rushing_yards)
        pack_stats(stats[home_team]['rushing']['yards'], home_rushing_yards, away_rushing_yards)
        ncaa_stats[game_key][away_team]['rushing yards'] = away_rushing_yards
        ncaa_stats[game_key][home_team]['rushing yards'] = home_rushing_yards

        # Rushing attempts
        rushing_attempts = file.readline().strip().split(',')
        away_rushing_attempts = int(rushing_attempts[0])
        home_rushing_attempts = int(rushing_attempts[1])
        pack_stats(stats[away_team]['rushing']['attempts'], away_rushing_attempts, home_rushing_attempts)
        pack_stats(stats[home_team]['rushing']['attempts'], home_rushing_attempts, away_rushing_attempts)
        ncaa_stats[game_key][away_team]['rushing attempts'] = away_rushing_attempts
        ncaa_stats[game_key][home_team]['rushing attempts'] = home_rushing_attempts

        # Passing yards
        passing_yards = file.readline().strip().split(',')
        away_passing_yards = int(passing_yards[0])
        home_passing_yards = int(passing_yards[1])
        pack_stats(stats[away_team]['passing']['yards'], away_passing_yards, home_passing_yards)
        pack_stats(stats[home_team]['passing']['yards'], home_passing_yards, away_passing_yards)
        ncaa_stats[game_key][away_team]['passing yards'] = away_passing_yards
        ncaa_stats[game_key][home_team]['passing yards'] = home_passing_yards

        # Passing attempts
        passing_attempts = file.readline().strip().split(',')
        away_passing_attempts = int(passing_attempts[0])
        home_passing_attempts = int(passing_attempts[1])
        pack_stats(stats[away_team]['passing']['attempts'], away_passing_attempts, home_passing_attempts)
        pack_stats(stats[home_team]['passing']['attempts'], home_passing_attempts, away_passing_attempts)
        ncaa_stats[game_key][away_team]['passing attempts'] = away_passing_attempts
        ncaa_stats[game_key][home_team]['passing attempts'] = home_passing_attempts

        # Passing completions
        passing_completions = file.readline().strip().split(',')
        away_passing_completions = int(passing_completions[0])
        home_passing_completions = int(passing_completions[1])
        pack_stats(stats[away_team]['passing']['completions'], away_passing_completions, home_passing_completions)
        pack_stats(stats[home_team]['passing']['completions'], home_passing_completions, away_passing_completions)
        ncaa_stats[game_key][away_team]['passing completions'] = away_passing_completions
        ncaa_stats[game_key][home_team]['passing completions'] = home_passing_completions

        # Total offensive yards
        total_offensive_yards = file.readline().strip().split(',')
        away_total_offensive_yards = int(total_offensive_yards[0])
        home_total_offensive_yards = int(total_offensive_yards[1])
        pack_stats(stats[away_team]['total offense']['yards'], away_total_offensive_yards, home_total_offensive_yards)
        pack_stats(stats[home_team]['total offense']['yards'], home_total_offensive_yards, away_total_offensive_yards)
        ncaa_stats[game_key][away_team]['total yards'] = away_total_offensive_yards
        ncaa_stats[game_key][home_team]['total yards'] = home_total_offensive_yards

        # Total offensive plays
        total_offensive_plays = file.readline().strip().split(',')
        away_total_offensive_plays = int(total_offensive_plays[0])
        home_total_offensive_plays = int(total_offensive_plays[1])
        pack_stats(stats[away_team]['total offense']['plays'], away_total_offensive_plays, home_total_offensive_plays)
        pack_stats(stats[home_team]['total offense']['plays'], home_total_offensive_plays, away_total_offensive_plays)
        ncaa_stats[game_key][away_team]['total plays'] = away_total_offensive_plays
        ncaa_stats[game_key][home_team]['total plays'] = home_total_offensive_plays

        # Total fumbles
        total_fumbles = file.readline().strip().split(',')
        away_total_fumbles = int(total_fumbles[0])
        home_total_fumbles = int(total_fumbles[1])
        pack_stats(stats[away_team]['fumbles']['total'], away_total_fumbles, home_total_fumbles)
        pack_stats(stats[home_team]['fumbles']['total'], home_total_fumbles, away_total_fumbles)
        ncaa_stats[game_key][away_team]['fumbles'] = away_total_fumbles
        ncaa_stats[game_key][home_team]['fumbles'] = home_total_fumbles

        # Lost fumbles
        lost_fumbles = file.readline().strip().split(',')
        away_lost_fumbles = int(lost_fumbles[0])
        home_lost_fumbles = int(lost_fumbles[1])
        pack_stats(stats[away_team]['fumbles']['lost'], away_lost_fumbles, home_lost_fumbles)
        pack_stats(stats[home_team]['fumbles']['lost'], home_lost_fumbles, away_lost_fumbles)
        ncaa_stats[game_key][away_team]['fumbles lost'] = away_lost_fumbles
        ncaa_stats[game_key][home_team]['fumbles lost'] = home_lost_fumbles

        # Penalties
        penalties = file.readline().strip().split(',')
        away_penalties = int(penalties[0])
        home_penalties = int(penalties[1])
        pack_stats(stats[away_team]['penalties']['number'], away_penalties, home_penalties)
        pack_stats(stats[home_team]['penalties']['number'], home_penalties, away_penalties)
        ncaa_stats[game_key][away_team]['penalties'] = away_penalties
        ncaa_stats[game_key][home_team]['penalties'] = home_penalties

        # Penalty yards
        penalty_yards = file.readline().strip().split(',')
        away_penalty_yards = int(penalty_yards[0])
        home_penalty_yards = int(penalty_yards[1])
        pack_stats(stats[away_team]['penalties']['yards'], away_penalty_yards, home_penalty_yards)
        pack_stats(stats[home_team]['penalties']['yards'], home_penalty_yards, away_penalty_yards)
        ncaa_stats[game_key][away_team]['penalty yards'] = away_penalty_yards
        ncaa_stats[game_key][home_team]['penalty yards'] = home_penalty_yards

        # Punts
        punts = file.readline().strip().split(',')
        away_punts = int(punts[0])
        home_punts = int(punts[1])
        pack_stats(stats[away_team]['punts']['number'], away_punts, home_punts)
        pack_stats(stats[home_team]['punts']['number'], home_punts, away_punts)
        ncaa_stats[game_key][away_team]['punts'] = away_punts
        ncaa_stats[game_key][home_team]['punts'] = home_punts

        # Punt yards
        punt_yards = file.readline().strip().split(',')
        away_punt_yards = int(punt_yards[0])
        home_punt_yards = int(punt_yards[1])
        pack_stats(stats[away_team]['punts']['yards'], away_punt_yards, home_punt_yards)
        pack_stats(stats[home_team]['punts']['yards'], home_punt_yards, away_punt_yards)
        ncaa_stats[game_key][away_team]['punt yards'] = away_punt_yards
        ncaa_stats[game_key][home_team]['punt yards'] = home_punt_yards

        # Punt returns
        punt_returns = file.readline().strip().split(',')
        away_punt_returns = int(punt_returns[0])
        home_punt_returns = int(punt_returns[1])
        pack_stats(stats[away_team]['punt returns']['number'], away_punt_returns, home_punt_returns)
        pack_stats(stats[home_team]['punt returns']['number'], home_punt_returns, away_punt_returns)
        ncaa_stats[game_key][away_team]['punt returns'] = away_punt_returns
        ncaa_stats[game_key][home_team]['punt returns'] = home_punt_returns

        # Punt return yards
        punt_return_yards = file.readline().strip().split(',')
        away_punt_return_yards = int(punt_return_yards[0])
        home_punt_return_yards = int(punt_return_yards[1])
        pack_stats(stats[away_team]['punt returns']['yards'], away_punt_return_yards, home_punt_return_yards)
        pack_stats(stats[home_team]['punt returns']['yards'], home_punt_return_yards, away_punt_return_yards)
        ncaa_stats[game_key][away_team]['punt return yards'] = away_punt_return_yards
        ncaa_stats[game_key][home_team]['punt return yards'] = home_punt_return_yards
        
        # Kickoff returns
        kickoff_returns = file.readline().strip().split(',')
        away_kickoff_returns = int(kickoff_returns[0])
        home_kickoff_returns = int(kickoff_returns[1])
        pack_stats(stats[away_team]['kickoff returns']['number'], away_kickoff_returns, home_kickoff_returns)
        pack_stats(stats[home_team]['kickoff returns']['number'], home_kickoff_returns, away_kickoff_returns)
        ncaa_stats[game_key][away_team]['kickoff returns'] = away_kickoff_returns
        ncaa_stats[game_key][home_team]['kickoff returns'] = home_kickoff_returns

        # Kickoff return yards
        kickoff_return_yards = file.readline().strip().split(',')
        away_kickoff_return_yards = int(kickoff_return_yards[0])
        home_kickoff_return_yards = int(kickoff_return_yards[1])
        pack_stats(stats[away_team]['kickoff returns']['yards'], away_kickoff_return_yards, home_kickoff_return_yards)
        pack_stats(stats[home_team]['kickoff returns']['yards'], home_kickoff_return_yards, away_kickoff_return_yards)
        ncaa_stats[game_key][away_team]['kickoff return yards'] = away_kickoff_return_yards
        ncaa_stats[game_key][home_team]['kickoff return yards'] = home_kickoff_return_yards

        # Interception returns
        interception_returns = file.readline().strip().split(',')
        away_interception_returns = int(interception_returns[0])
        home_interception_returns = int(interception_returns[1])
        pack_stats(stats[away_team]['interception returns']['number'], away_interception_returns, home_interception_returns)
        pack_stats(stats[home_team]['interception returns']['number'], home_interception_returns, away_interception_returns)
        ncaa_stats[game_key][away_team]['passing interceptions'] = home_interception_returns
        ncaa_stats[game_key][home_team]['passing interceptions'] = away_interception_returns
        ncaa_stats[game_key][away_team]['interception returns'] = away_interception_returns
        ncaa_stats[game_key][home_team]['interception returns'] = home_interception_returns


        # Interception return yards
        interception_return_yards = file.readline().strip().split(',')
        away_interception_return_yards = int(interception_return_yards[0])
        home_interception_return_yards = int(interception_return_yards[1])
        pack_stats(stats[away_team]['interception returns']['yards'], away_interception_return_yards, home_interception_return_yards)
        pack_stats(stats[home_team]['interception returns']['yards'], home_interception_return_yards, away_interception_return_yards)
        ncaa_stats[game_key][away_team]['interception return yards'] = away_interception_return_yards
        ncaa_stats[game_key][home_team]['interception return yards'] = home_interception_return_yards

        # Third down conversions
        third_down_conversions = file.readline().strip().split(',')
        away_third_down_conversions = int(third_down_conversions[0])
        home_third_down_conversions = int(third_down_conversions[1])
        pack_stats(stats[away_team]['3rd downs']['conversions'], away_third_down_conversions, home_third_down_conversions)
        pack_stats(stats[home_team]['3rd downs']['conversions'], home_third_down_conversions, away_third_down_conversions)
        ncaa_stats[game_key][away_team]['3rd down conversions'] = away_third_down_conversions
        ncaa_stats[game_key][home_team]['3rd down conversions'] = home_third_down_conversions

        # Third down attempts
        third_down_attempts = file.readline().strip().split(',')
        away_third_down_attempts = int(third_down_attempts[0])
        home_third_down_attempts = int(third_down_attempts[1])
        pack_stats(stats[away_team]['3rd downs']['attempts'], away_third_down_attempts, home_third_down_attempts)
        pack_stats(stats[home_team]['3rd downs']['attempts'], home_third_down_attempts, away_third_down_attempts)
        ncaa_stats[game_key][away_team]['3rd down attempts'] = away_third_down_attempts
        ncaa_stats[game_key][home_team]['3rd down attempts'] = home_third_down_attempts

        # Fourth down conversions
        fourth_down_conversions = file.readline().strip().split(',')
        away_fourth_down_conversions = int(fourth_down_conversions[0])
        home_fourth_down_conversions = int(fourth_down_conversions[1])
        pack_stats(stats[away_team]['4th downs']['conversions'], away_fourth_down_conversions, home_fourth_down_conversions)
        pack_stats(stats[home_team]['4th downs']['conversions'], home_fourth_down_conversions, away_fourth_down_conversions)
        ncaa_stats[game_key][away_team]['4th down conversions'] = away_fourth_down_conversions
        ncaa_stats[game_key][home_team]['4th down conversions'] = home_fourth_down_conversions

        # Fourth down attempts
        fourth_down_attempts = file.readline().strip().split(',')
        away_fourth_down_attempts = int(fourth_down_attempts[0])
        home_fourth_down_attempts = int(fourth_down_attempts[1])
        pack_stats(stats[away_team]['4th downs']['attempts'], away_fourth_down_attempts, home_fourth_down_attempts)
        pack_stats(stats[home_team]['4th downs']['attempts'], home_fourth_down_attempts, away_fourth_down_attempts)
        ncaa_stats[game_key][away_team]['4th down attempts'] = away_fourth_down_attempts
        ncaa_stats[game_key][home_team]['4th down attempts'] = home_fourth_down_attempts

        # Check if the week is 'bowl' week
        num_weeks = read_number_of_weeks(year)
        week, _ = utils.check_week(week, num_weeks)
            
        # Create the stats file with absolute path
        absolute_path = utils.get_abs_path(__file__)
        filename = '{0}/{1}/stats-{1}-{2:02}.json'.format(absolute_path, year, week)
        utils.write_json(stats, filename)
            
        # Create the ncaa stats file with absolute path
        absolute_ncaa_path = utils.get_abs_path(__file__)
        ncaa_filename = '{0}/{1}/ncaa/ncaa_stats-{1}-{2:02}.json'.format(absolute_ncaa_path, year, week)
        utils.write_json(ncaa_stats, ncaa_filename)

def pack_stats(stats, new_gained_stats, new_allowed_stats):

    # Pack gained stats
    stats['gained'][-1] = new_gained_stats

    # Pack allowed stats
    stats['allowed'][-1] = new_allowed_stats


if __name__ == '__main__':
    year = int(sys.argv[1])
    week = sys.argv[2]
    if week != 'bowl':
        week = int(week)
    away_team = sys.argv[3]
    home_team = sys.argv[4]
    patch_stats(year, week, away_team, home_team)
    print('Finished patching stats...')
