# TheKickIsBAD: An open-source NCAA football statistics engine.
# Copyright (C) 2019  BAD Engineering LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, realpath
import sys
root = dirname(dirname(realpath(__file__)))
sys.path.append(root)

# Standard imports
import time
from urllib.request import urlopen

# TheKickIsBAD imports
import tools.utils as utils
from scores.read_number_of_weeks import read_number_of_weeks


def download_scores(year, week, num_retries=3):

    # Check if the week is 'bowl' week and set the download URL
    num_weeks = read_number_of_weeks(year)
    week, is_bowl = utils.check_week(week, num_weeks)
    if is_bowl:
        url = 'https://data.ncaa.com/casablanca/scoreboard/football/fbs/{0}/P/scoreboard.json'.format(year)
    else:
        url = 'https://data.ncaa.com/casablanca/scoreboard/football/fbs/{0}/{1:02}/scoreboard.json'.format(year, week)

    print('Downloading scores year {0}, week {1:02}...'.format(year, week))

    # Download the scores as json
    success = False
    retries = 0
    while retries < num_retries:
        try:
            web_response = urlopen(url)
            web_data = web_response.read()
            success = True
            break
        except Exception:
            print('......Error downloading scores, retrying...')
            retries += 1
            time.sleep(1)
    if not success:
        raise Exception('Failed to download scores')

    # Create the scores file with absolute path
    absolute_path = utils.get_abs_path(__file__)
    filename = '{0}/{1}/scores-{1}-{2:02}.json'.format(absolute_path, year, week)
    utils.write_bytes(web_data, filename)


if __name__ == '__main__':
    year = int(sys.argv[1])
    week = sys.argv[2]
    if week != 'bowl':
        week = int(week)
    download_scores(year, week)
