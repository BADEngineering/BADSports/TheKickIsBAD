# TheKickIsBAD: An open-source NCAA football statistics engine.
# Copyright (C) 2019  BAD Engineering LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, realpath
import sys
root = dirname(dirname(realpath(__file__)))
sys.path.append(root)

# TheKickIsBAD imports
import tools.utils as utils
from scores.read_number_of_weeks import read_number_of_weeks
from teams.get_standard_team_name import get_standard_team_name
from teams.read_teams import read_teams


def read_scores(year, week):

    # Check if the week is 'bowl' week
    num_weeks = read_number_of_weeks(year)
    week, _ = utils.check_week(week, num_weeks)

    # Open scores file with absolute path
    absolute_path = utils.get_abs_path(__file__)
    filename = '{0}/{1}/scores-{1}-{2:02}.json'.format(absolute_path, year, week)
    
    # Read the raw scores directly from json
    scores = utils.read_json(filename)

    # Read the teams list for checking team names in scores
    teams, _ = read_teams(year)

    # Loop through each game and standardize the team names
    for game in scores['games']:
        
        # Standardize away team name, checking for FCS
        away_team_nickname = game['game']['away']['names']['short']
        away_team = get_standard_team_name(away_team_nickname)
        if away_team not in teams:
            away_team = 'FCS'
        game['game']['away']['names']['standard'] = away_team

        # Standardize home team name, checking for FCS
        home_team_nickname = game['game']['home']['names']['short']
        home_team = get_standard_team_name(home_team_nickname)
        if home_team not in teams:
            home_team = 'FCS'
        game['game']['home']['names']['standard'] = home_team

    return scores
