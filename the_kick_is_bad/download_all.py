from scores.download_scores import download_scores
from scores.read_number_of_weeks import read_number_of_weeks
from stats.download_stats import download_stats

# for year in range(2013, 2014):
#     num_weeks = read_number_of_weeks(year)
#     for week in range(4, num_weeks + 1):
#         download_stats(year, week)
#         # download_scores(year, week)
#     download_stats(year, 'bowl')
#     # download_scores(year, 'bowl')

for year in range(2013, 2020):
    num_weeks = read_number_of_weeks(year)
    for week in range(1, num_weeks + 1):
        download_stats(year, week)
        download_scores(year, week)
    download_stats(year, 'bowl')
    download_scores(year, 'bowl')
