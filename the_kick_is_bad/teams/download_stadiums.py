# TheKickIsBAD: An open-source NCAA football statistics engine.
# Copyright (C) 2019  BAD Engineering LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, realpath
import sys
root = dirname(dirname(realpath(__file__)))
sys.path.append(root)

# Standard imports
import json
import time
from collections import Counter as counter
from urllib.request import urlopen

# TheKickIsBAD imports
import tools.utils as utils
from scores.read_number_of_weeks import read_number_of_weeks
from scores.read_scores import read_scores
from teams.read_teams import read_teams


def download_stadiums(year):

    print('Downloading stadiums from ncaa.com...')

    stadiums = {}

    # Read the teams and initialize a stadium array for each team
    teams, _ = read_teams(year)
    for team in teams:
        stadiums[team] = []
    stadiums['FCS'] = ['FCS-FCS-FCS']

    # Loop through all weeks to download game info
    # Avoid week (num_weeks - 1) as it is championship week and not populated ahead of time
    num_weeks = read_number_of_weeks(year)
    for week in range(1, num_weeks - 1):

        scores = read_scores(year, week)

        # Loop through all games to save location info
        num_games = len(scores['games'])
        i_game = 1
        for game in scores['games']:

            progress_string = '...Downloading stadium for week {0}, game {1} of {2}...'.format(week, i_game, num_games)
            print(progress_string, end='\r')

            # Skip FCS, it doesn't have a common home stadium
            home_team = game['game']['home']['names']['standard']
            if home_team == 'FCS':
                continue

            # Download the game info using the game center URL
            game_center_url = game['game']['url']
            game_info_url = 'https://data.ncaa.com/casablanca{0}/gameInfo.json'.format(game_center_url)
            num_retries = 3
            success = False
            while num_retries > 0:
                try:
                    game_info = download_game_info(game_info_url)
                    num_retries = 0
                    success = True
                except Exception:
                    print('......Error downloading game info, retrying...')
                    num_retries -= 1
                    time.sleep(1)
            if not success:
                raise Exception('Failed to download game info')
            
            # Save the stadium name, including city/state in case there are repeat stadium names
            translation_table = str.maketrans('', '', '/-&() .;\'\n\t\r')
            stadium = game_info['venue']['name'].translate(translation_table)
            city = game_info['venue']['city'].translate(translation_table)
            state = game_info['venue']['state'].translate(translation_table)
            stadiums[home_team].append('{0}-{1}-{2}'.format(stadium, city, state))

            i_game += 1

    # Print a new line since the progress string used carriage returns
    print('')

    # Find the most common stadium for each team
    for team in teams:
        if not stadiums[team]:
            stadiums[team] = '{0}-{0}-{0}'.format(team)
        else:
            stadiums[team] = counter(stadiums[team]).most_common(1)[0][0]
        
    # Create the stadiums file with absolute path
    absolute_path = utils.get_abs_path(__file__)
    filename = '{0}/{1}/home_stadiums-{1}.json'.format(absolute_path, year)
    utils.write_json(stadiums, filename)

def download_game_info(game_info_url):
    web_response = urlopen(game_info_url)
    web_data = web_response.read()
    game_info = json.loads(web_data)
    return game_info

if __name__ == '__main__':
    year = int(sys.argv[1])
    download_stadiums(year)
