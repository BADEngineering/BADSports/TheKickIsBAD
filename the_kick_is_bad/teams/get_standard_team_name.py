# TheKickIsBAD: An open-source NCAA football statistics engine.
# Copyright (C) 2019  BAD Engineering LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, realpath
import sys
root = dirname(dirname(realpath(__file__)))
sys.path.append(root)

# TheKickIsBAD imports
import tools.utils as utils


def get_standard_team_name(team_name):

    # Remove punctuation, spaces, and special characters
    translation_table = str.maketrans('', '', '/-&() .;\'\n\t\r')
    team_name = team_name.translate(translation_table)
    
    # Read alternate team names from file and check for match
    alternate_team_names = read_alternate_team_names()
    if team_name in alternate_team_names:
        team_name = alternate_team_names[team_name]

    return team_name

def read_alternate_team_names():

    # Open alternate team names file with absolute path
    absolute_path = utils.get_abs_path(__file__)
    filename = '{0}/alternate_team_names.txt'.format(absolute_path)
    with open(filename) as file:

        alternate_team_names = {}

        # Read the current team name
        team_name = file.readline().strip()

        # Loop through team names in the file to find the standard name
        standard_team_name = ''
        while team_name:

            # Nicknames are marked by a leading '-', remaining are standard names
            if '-' in team_name:

                # Remove the '-' from the string
                team_name = team_name.translate(str.maketrans('', '', '-'))

                # Save the standard team name for this nickname
                # File order guarantees standard name is already set
                alternate_team_names[team_name] = standard_team_name
            else:
                standard_team_name = team_name

            # Read the next team name
            team_name = file.readline().strip()
            
        return alternate_team_names
