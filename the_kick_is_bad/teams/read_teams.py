# TheKickIsBAD: An open-source NCAA football statistics engine.
# Copyright (C) 2019  BAD Engineering LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, realpath
import sys
root = dirname(dirname(realpath(__file__)))
sys.path.append(root)

# TheKickIsBAD imports
import tools.utils as utils


def read_teams(year):

    # Open teams file with absolute path
    absolute_path = utils.get_abs_path(__file__)
    filename = '{0}/{1}/teams-{1}.json'.format(absolute_path, year)
    
    # Read the conference->division->team structure directly from json
    teams_by_conference = utils.read_json(filename)

    # Build the teams list from the conference structure
    teams = {}
    for conference in teams_by_conference:
        for division in teams_by_conference[conference]:
            for team in teams_by_conference[conference][division]:
                teams[team] = {}
                teams[team]['division'] = division
                teams[team]['conference'] = conference

    return teams, teams_by_conference
