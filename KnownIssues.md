# Known issues

## Wrong conference values on ncaa.com

When looking at past years' games on ncaa.com, the conference value for each team is the team's
current conference, not the team's conference at the time of the game. This has the side effect
that games are shown on the past years' scoreboards for teams that were not FBS at the time of
playing, such as Liberty, Coastal Carolina, Charlotte, Georgia Southern, and Appalachian State, and
these games usually don't have statistics available to download. Games where these teams play other
FCS teams need to be manually removed from the scores file after a download.

Similarly, teams that have left the FBS, currently only Idaho, no longer have games shown on past
years' scoreboards, because their conference is marked as FCS. This can be manually changed to FBS
in the scores file URLs after a download, as the statistics pages still exist.

## Missing historical data on ncaa.com

Some games' statistics pages are simply missing on ncaa.com, even if the game is valid in all other
aspects. There is no workaround; these games are removed from the scores file as if they did not
happen.

- 2013-01: Towson @ UConn (gameInfo exists, teamStats does not)
- 2013-02: Alcorn St @ Mississippi St (gameInfo exists, teamStats does not)
- 2013-03: Fresno St @ Colorado (gameInfo exists, teamStats does not)
- 2013-04: Michigan @ UConn (everything exists, web read fails anyway)
  - Stats are included because once in awhile it does succeed
- 2013-05: UConn @ Buffalo (gameInfo exists, teamStats does not)
- 2013-16: Army @ Navy (gameInfo exists, teamStats does not)
- 2014-03: Arizona @ UTSA (everything exists, statistics are not populated)
- 2014-03: Middle Tennessee @ Minnesota (everything exists, statistics are not populated)
- 2014-03: Wagner @ FIU (everything exists, statistics are not populated)
- 2014-03: Alcorn St @ Southern Miss (everything exists, statistics are not populated)
- 2014-03: BYU @ Texas (everything exists, statistics are not populated)
- 2014-04: UTSA @ Oklahoma St (everything exists, statistics are not populated)
- 2014-04: Tulsa @ Fla Atlantic (everything exists, statistics are not populated)
- 2015-07: Northern Ill @ Miami OH (everything exists, statistics are not populated)
- 2015-10: Mississippi St @ Missouri (everything exists, statistics are not populated)
- 2015-11: Utah @ Arizona (everything exists, statistics are not populated)
- 2016-05: Alcorn St @ Arkansas (gameInfo exists, teamStats does not)
- 2017-02: Alcorn St @ FIU (gameInfo exists, teamStats does not)

Games from 2018 or later that are missing might exist with the game ID URL format instead of the
traditional format. These can be corrected manually in the scores file URLs after a new download.

## Multi-day games counted twice

It seems games that go past midnight are given two games entries, only one of which has valid
statistics pages. The second one must be manually deleted from the scores file after a download.

- 2013-05: Fresno St @ Hawaii
- 2013-09: Colorado St @ Hawaii
