# TheKickIsBAD

TheKickIsBAD is an open-source NCAA football statistics engine. It downloads game scores and statistics, calculates additional derived data, and saves everything to json files. A simple API is provided to read this data, allowing applications to be created on top of TheKickIsBad that predict game outcomes, rank teams, identify interesting trends, etc.

The downloading of each week's data is handled by me, and in the future automated by CI/CD, so application projects using TheKickIsBAD should not need to use the download functions if they configure TheKickIsBAD as a git submodule - just update to the latest commit on master each week. If the project is copied offline and the download functions need to be run, see the API descriptions below.

TheKickIsBAD has gone through several iterations since 2013 (all in Matlab before this version) and is now released as an open source project by my company BAD Engineering LLC, under the GPLv3 license. Two reference application projects are implemented, both evolving since 2013 along with the base platform. Hopefully you find some inspiration in these examples to make your own program!

Dynamite Rankings

- [Repository on Github](https://github.com/arajala/DynamiteRankings)
- [Twitter - analysis results](https://twitter.com/dynarankings)
- [How it Works (Version 2.0)](http://dynamiterankings.blogspot.com/2013/08/how-it-works-version-20.html)

SkunkWorks

- Uses neural networks, still being reimplemented in Python for 2019.

## Scores

Game scores and metadata are downloaded from ncaa.com, and saved to scores/\<year>/scores-\<year>-\<week>.json files. The files contain team names, scores, game state, start date, start time, and a few other data fields.

At the start of each year, the file scores/\<year>/number_of_weeks-\<year>.txt must created. The file should only contain the number of regular season weeks the upcoming football season will have on line 1, and an empty line 2. The length of the season in weeks varies from year to year, so this file allows automated processing to correctly handle the season length.

### read_scores.py

Read the scores for a given week from the json file and return a python dictionary.

    # <week> is either the regular season week number (1, 2, 3, ...) or "bowl"
    import the_kick_is_bad
    scores = the_kick_is_bad.read_scores(<year>, <week>)

The return dictionary adds an additional field called 'standard' to the 'names' field. The standard name can be used as a key to all other dictionaries that are arranged by team name.

### download_scores.py

`Note: Configure TheKickIsBAD as a git submodule and you will not need to run this!`

Download the latest game scores and metadata and save everything to json files.

    # <week> is either the regular season week number (1, 2, 3, ...) or "bowl"
    python download_scores.py <year> <week>

Alternatively, use the provided .vscode/launch.json configurations with Visual Studio Code and just configure the year and week arguments.

## Statistics

Team statistics are downloaded primarily from ncaa.com, and secondarily from teamrankings.com. The teamrankings.com statistics include a wider range of categories, but are only counted for games between two FBS teams, whereas ncaa.com statistics will also count games against an FCS opponent. All statistics are saved in the stats/\<year>/stats-\<year>-\<week>.json files.

The categories of statistics currently downloaded (both offensive/gained and defensive/allowed) are:

- Points (total, by quarter, by half)
- First downs (total, rushing, passing, penalty)
- Rushing (yards, attempts)
- Passing (yards, attempts, completions, interceptions)
- Total offense (yards, plays, touchdowns, points)
- Fumbles (total, lost)
- Penalties (number, yards)
- Punts (number, yards)
- Punt returns (number, yards)
- Kickoff returns (number, yards)
- Interception returns (number, yards)
- Third downs (attempts, conversions)
- Fourth downs (attempts, conversions)
- Red zone trips (attempts, conversions)
- Time of posession percentage (total, by quarter, by half)
- Sacks
- Team passer rating
- Field goals (attempts, made)

The additional derived statistics currently are:

- Games played
- Win/loss record
- Opponents' win/loss record
- Opponents' opponents' win/loss record

For every statistic, these splits are calculated:

- Season total
- In the last 3 games
- In home games
- In away games
- vs conference opponents
- vs winning record opponents
- vs losing record opponents (including 0.500 records)

### download_stats.py

`Note: Configure TheKickIsBAD as a git submodule and you will not need to run this!`

Download the latest team statistics and calculate derived statistics and save everything to json files.

    # <week> is either the regular season week number (1, 2, 3, ...) or "bowl"
    python download_stats.py <year> <week>

Alternatively, use the provided .vscode/launch.json configurations with Visual Studio Code and just configure the year and week arguments.

### read_stats.py

Read the statistics for a given week from the json file and return a python dictionary.

    # <week> is either the regular season week number (1, 2, 3, ...) or "bowl"
    import the_kick_is_bad
    stats = the_kick_is_bad.read_stats(<year>, <week>)

## Teams

A complete list of conferences, divisions, and teams is manually maintained at the start of each football season, and stored in the teams/\<year>/teams-\<year>.json files. [Wikipedia](https://en.wikipedia.org/wiki/2019_NCAA_Division_I_FBS_football_season) maintains good football season pages, which detail any conference realignment moves that need to be changed, compared to the previous year's file. Each team's home stadium is also stored, to better allow handling of true home games vs neutral site games.

### download_stadiums.py

`Note: Configure TheKickIsBAD as a git submodule and you will not need to run this!`

Download the detailed schedule for the whole year in advance, find the most common home stadium for each team, and save the home stadiums to a json file.

    python download_stadiums.py <year>

### get_standard_team_name.py

In case you have a team name string which may not match the keys of the standard dictionaries, this function trims unwanted characters, and makes substitutions for things such as "st" for "state", and other abbreviations. The file teams/alternate_team_names.txt is maintained with common alternate spellings of team names that are translated when this function is called.

    import the_kick_is_bad
    team_name = the_kick_is_bad.get_standard_team_name(<raw_team_name_string>)

### read_teams.py

Read the teams data from the json file and return two python dictionaries. "teams" is an array of team dictionaries, each of which includes that team's conference and division. "teams_by_conference" is an array of conference dictionaries, each of which include that conference's divisions, which in turn include that division's teams.

    import the_kick_is_bad
    teams, teams_by_conference = the_kick_is_bad.read_teams(<year>)
